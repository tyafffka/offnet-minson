#### версия 1.8.0:

- Добавлены функции `minsonPrettySerialize`, `minsonPrettySerializeFile`,
  `minsonPrettySerializeStream` (`serialize...(is_pretty_)` для C++, Python,
  `MinSONObject.prettySerialize`, `MinSONObject.prettySerializeFile` для
  Java) — вывод с отступами и переносами строк.

#### версия 1.7.0:

- Добавлено больше символов, которые можно использовать в идентификаторах:
  * `. _ - ~ : / @ $ ! ? + *`.
- Добавлена поддержка Python версии 3:
  * опция сборки `PYTHON_VERSION` для указания версии Python.

#### версия 1.6.1:

- Исправлены утечки памяти/двойные удаления при парсинге.
- Добавлены специальные символы для escape-последовательностей.
- Место ошибки во входных данных теперь указывается в виде "строка, столбец".

#### версия 1.6.0:

- Добавлена функция `minsonArrayUpdate` (`minson::array::update` для C++,
  `MinSONArray.update` для Python и Java).

#### версия 1.5.0:

- Добавлена функция `minsonArrayGetIter` (`minson::array::find` и
  `minson::array::rfind` для C++, `MinSONArray.iterfrom` для Python,
  `MinSONArray.find` и `MinSONArray.rfind` для Java).

#### версия 1.4.3:

- Изменено хеширование в массиве, чтобы исключить совпадение двух ячеек у
  одного элемента.

#### версия 1.4.2:

- Исправления итераторов в Python, Java.

#### версия 1.4.1:

- Небольшие исправления Java-обёртки.

#### версия 1.4.0:

- `minsonDuplicateArray`, `minsonSerializeStream`, `minsonSerializeFile` —
  логическое дополнение интерфейса библиотеки.
- Прикладной интерфейс для Python.
