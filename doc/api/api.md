Программный интерфейс библиотеки
--------------------------------

Библиотека написана на языке **C** и предоставляет структуры и функции для
взаимодействия с MinSON-объектами, а также их сериализации и десериализации.

Описание программного интерфейса находится [здесь](base.md). Для того, чтобы
воспользоваться функциями библиотеки, необходимо подключить заголовочный файл
_`minson.h`_.

--------------------------------

Имеется интерфейс для языка **C++**, описаный [здесь](cpp.md). Все классы и
функции находятся в пространстве имён `minson`. Чтобы воспользоваться
интерфейсом, необходимо подключить заголовочный файл _`minson.hpp`_.

--------------------------------

Также существует обёртка на языке **Java**, интерфейс которой описан
[здесь](java.md). Все классы находятся в пакете _`bred_soft.minson`_.

--------------------------------

Также имеется модуль-обёртка для языка **Python**, интерфейс которой описан
[здесь](python.md). Имя модуля — `minson`.
