Программный интерфейс библиотеки
--------------------------------

_Заголовочный файл: `minson.h`_

--------------------------------

### Работа с объектами

#### `enum c_minson_type;`

Возможные типы объектов.

#### `struct t_minson_object;`

Базовый MinSON-объект.

- `char const *name;` — имя объекта или `NULL`, если объект безымянный;
- `c_minson_type type;` — тип объекта.

#### `t_minson_object *minsonCreateString(char const *name_, char const *value_);`<br>`t_minson_object *minsonCreateNumber(char const *name_, int64_t value_);`<br>`t_minson_object *minsonCreateFloatNumber(char const *name_, double value_);`<br>`t_minson_object *minsonCreateBoolean(char const *name_, uint8_t value_);`<br>`t_minson_object *minsonCreateNull(char const *name_);`

Создают MinSON-объект с указанным значением соответствующего типа. Параметр
`name_` может быть `NULL`, в этом случае объект будет безымянным. Созданные
таким образом объекты следует удалять с помощью функции `minsonDelete`.

#### `char const *minsonGetString(t_minson_object *object_);`

Проверяет, что тип объекта — строка, и возвращает значение (указатель на
_внутренний_ буфер). В случае ошибок возвращает `NULL`.

#### `uint8_t minsonGetNumber(t_minson_object *object_, int64_t *out_value_);`<br>`uint8_t minsonGetFloatNumber(t_minson_object *object_, double *out_value_);`<br>`uint8_t minsonGetBoolean(t_minson_object *object_, uint8_t *out_value_);`

Проверяют тип объекта и записывают значение соответствующего типа по адресу,
указанному в `out_value_`. Возвращают ненулевое значение в случае успеха и
ноль в ином случае.

#### `uint8_t minsonIsNull(t_minson_object *object_);`

Проверяет, что тип объекта — пустой, и возвращает ненулевое значение в случае
успеха и ноль в ином случае.

#### `t_minson_object *minsonDuplicate(t_minson_object *object_);`

Создаёт копию объекта таким же образом, как это делают функции
`minsonCreate...`. Массивы копируются рекурсивно.

#### `void minsonDelete(t_minson_object *object_);`

Удаляет объект (очищает память), созданный с помощью функций `minsonCreate...`,
`minsonParse...` и `minsonDuplicate`. Массивы удаляются рекурсивно.

--------------------------------

### Работа с массивами

#### `struct t_minson_array;`

Объект MinSON-массива.

- `t_minson_object h;` — заголовок наследования;

#### `t_minson_array *minsonCreateArray(char const *name_);`

Создаёт объект массива с указанным именем (или безымянный, если `name_` равно
`NULL`). Объект следует удалять с помощью функции `minsonDelete`.

#### `t_minson_array *minsonCastArray(t_minson_object *object_);`

Проверяет, что тип объекта — массив, и возвращает указатель на массив (тот же
адрес) в случае успеха и `NULL` в ином случае.

#### `t_minson_object *minsonUpcast(t_minson_array *array_);`

Преобразует указатель на массив к указателю на объект.

#### `t_minson_array *minsonDuplicateArray(t_minson_array *array_);`

Создаёт рекурсивную копию массива. Эквивалентно
`minsonCastArray(minsonDuplicate(minsonUpcast(array_)))`.

#### `void minsonDeleteArray(t_minson_array *array_);`

Удаляет объект по указателю на массив. Эквивалентно
`minsonDelete(minsonUpcast(array_))`.

#### `void minsonArrayInit(t_minson_array *this_);`

Инициализирует структуру, на которую указывает `this_`. _НЕ_ требуется, если
объект создан с помощью `minsonCreateArray`.

#### `void minsonArrayClear(t_minson_array *this_);`

Деинициализирует структуру (волженные объекты при этом _НЕ_ удаляются). _НЕ_
требуется, если объект удаляется с помощью `minsonDelete` или
`minsonDeleteArray`.

#### `void minsonArrayAdd(t_minson_array *this_, t_minson_object *object_);`

Помещает объект в конец массива `this_`.

#### `t_minson_object *minsonArrayGet(t_minson_array const *this_, char const *name_);`

Ищет объект в массиве по имени и возвращает его в случае успеха, иначе
возвращает `NULL`.

#### `t_minson_object *minsonArrayRemove(t_minson_array *this_, char const *name_);`

Ищет объект в массиве по имени и извлекает его из массива. Возвращает
извлечённый объект.

#### `void minsonArrayUpdate(t_minson_array *this_, t_minson_array *from_);`

Дополняет массив, копируя объекты из массива `from_` (использует
`minsonDuplicate`). Именованные объекты заменяются (старый объект удаляется с
помощью `minsonDelete`). Рекурсивно для вложенных массивов.

#### `uint32_t minsonArrayCount(t_minson_array *this_);`

_Макрос_. Возвращает общее число объектов, вложенных в массив.

#### `uint32_t minsonArrayNamedCount(t_minson_array *this_);`

_Макрос_. Возвращает число объектов, имеющих имя.

#### `void minsonArrayAddArray(t_minson_array *this_, char const *name_);`<br>`void minsonArrayAddString(t_minson_array *this_, char const *name_, char const *value_);`<br>`void minsonArrayAddNumber(t_minson_array *this_, char const *name_, int64_t value_);`<br>`void minsonArrayAddFloatNumber(t_minson_array *this_, char const *name_, double value_);`<br>`void minsonArrayAddBoolean(t_minson_array *this_, char const *name_, uint8_t value_);`<br>`void minsonArrayAddNull(t_minson_array *this_, char const *name_);`

_Макросы_. Создают MinSON-объект с указанным значением соответствующего типа и
помещают его в конец указанного массива.

#### `t_minson_array *minsonArrayGetArray(t_minson_array *this_, char const *name_);`

_Макрос_. Ищет объект в массиве по имени и преобразует указатель на него в
указатель на массив. В случае неудачи возвращает `NULL`.

#### `char const *minsonArrayGetString(t_minson_array *this_, char const *name_);`

_Макрос_. Ищет объект в массиве по имени и возвращает строковое значение, если
тип объекта — строка. В случае неудачи возвращает `NULL`.

#### `uint8_t minsonArrayGetNumber(t_minson_array *this_, char const *name_, int64_t *out_value_);`<br>`uint8_t minsonArrayGetFloatNumber(t_minson_array *this_, char const *name_, double *out_value_);`<br>`uint8_t minsonArrayGetBoolean(t_minson_array *this_, char const *name_, uint8_t *out_value_);` 

_Макросы_. Ищут объект в массиве по имени и записывают значение
соответствующего типа в `out_value_`, если тип совпадает. Возвращают ненулевое
значение в случае успеха и ноль в ином случае.

#### `uint8_t minsonArrayIsNull(t_minson_array *this_, char const *name_);`

_Макрос_. Ищет объект в массиве по имени и проверяет, что тип объекта — пустой.
Возвращает ненулевое значение в случае успеха и ноль в ином случае.

--------------------------------

### Итерирование по массиву

#### `typedef ... t_minson_iter;`

Итератор по массиву.

#### `t_minson_iter minsonFirst(t_minson_array *array_);`

_Макрос_. Возвращает итератор, указывающий на первый элемент массива.

#### `t_minson_iter minsonLast(t_minson_array *array_);`

_Макрос_. Возвращает итератор, указывающий на последний элемент массива.

#### `bool minsonNotEnd(t_minson_iter v_iter);`

_Макрос_. Проверяет, достиг ли итератор конца и возвращает `false`, если
достиг и `true` если нет.

#### `t_minson_iter minsonNext(t_minson_iter v_iter);`

_Макрос_. _`v_iter` должен быть переменной_. Переместить итератор к
следующему элементу.

#### `t_minson_iter minsonPrev(t_minson_iter v_iter);`

_Макрос_. _`v_iter` должен быть переменной_. Переместить итератор к
предыдущему элементу.

#### `t_minson_object *minsonIterObject(t_minson_iter iter_);`

_Макрос_. Возвращает объект, на который указывает итератор.

#### `char const *minsonIterName(t_minson_iter iter_);`

_Макрос_. Возвращает имя объекта, на который указывает итератор.

#### `t_minson_iter minsonArrayGetIter(t_minson_array const *this_, char const *name_);`

Ищет в массиве объект по имени и возвращает итератор, который на него
указывает, в случае успеха, иначе возвращает `NULL`.

#### `t_minson_object *minsonIterRemove(t_minson_array *this_, t_minson_iter iter_);`

Извлекает объект, на который указывает итератор, из массива. Возвращает
извлечённый объект.

#### `t_minson_array *minsonIterGetArray(t_minson_iter iter_);`

_Макрос_. Преобразует указатель на объект внутри итератора в указатель на
массив. В случае неудачи возвращает `NULL`.

#### `char const *minsonIterGetString(t_minson_iter iter_);`

_Макрос_. Возвращает строковое значение, если тип объекта внутри итератора —
строка, или `NULL` в случае неудачи.

#### `uint8_t minsonIterGetNumber(t_minson_iter iter_, int64_t *out_value_);`<br>`uint8_t minsonIterGetFloatNumber(t_minson_iter iter_, double *out_value_);`<br>`uint8_t minsonIterGetBoolean(t_minson_iter iter_, uint8_t *out_value_);`

_Макросы_. Записывают значение соответствующего типа в `out_value_`, если тип
объекта внутри итератора совпадает. Возвращают ненулевое значение в случае
успеха и ноль в ином случае.

#### `uint8_t minsonIterIsNull(t_minson_iter iter_);`

_Макрос_. Проверяет, что тип объекта внутри итератора — пустой. Возвращает
ненулевое значение в случае успеха и ноль в ином случае.

--------------------------------

### Сериализация

#### `struct t_minson_string_builder;`

Объект строкового буфера. Накапливает отрезки для дальнейшего формирования
строки.

#### `t_minson_string_builder *minsonCreateStringBuilder();`

Создаёт объект строкового буфера, который следует удалять с помощью функции
`minsonDeleteStringBuilder`.

#### `void minsonDeleteStringBuilder(t_minson_string_builder *this_);`

Деинициализирует и удаляет объект строкового буфера.

#### `void minsonStringBuilderMove(t_minson_string_builder *this_, char const *str_, size_t str_size_);`

Поместить отрезок `str_` размера `str_size_` в строковый буфер. Память,
используемая отрезком, должна быть выделена с помощью функции `malloc`.
Владение этой памятью переходит к буферу.

#### `void minsonStringBuilderPutChar(t_minson_string_builder *this_, char ch_);`

Поместить символ `ch_` в строковый буфер.

#### `size_t minsonStringBuilderSize(t_minson_string_builder *this_);`

Возвращает размер строки, формируемой из накопленных отрезков.

#### `void minsonBuildString(t_minson_string_builder *this_, char *out_string_);`

Записывает полученную строку в `out_string_`, размер которого должен быть не
менее, чем значение, возвращаемое функцией `minsonStringBuilderSize`.
Завершающий ноль _НЕ_ ставится.

--------------------------------

#### `uint8_t minsonSerialize(t_minson_object *object_, t_minson_string_builder *out_);`

Формирует текстовое представление объекта, записывая его в строковый буфер
`out_`. Возвращает ненулевое значение в случае успеха и ноль — в случае ошибки.

#### `uint8_t minsonPrettySerialize(t_minson_object *object_, t_minson_string_builder *out_);`

Формирует текстовое представление объекта с отступами и переносами строк,
записывая его в строковый буфер `out_`. Возвращает ненулевое значение в случае
успеха и ноль — в случае ошибки.

#### `uint8_t minsonSerializeFile(t_minson_object *object_, char const *filename_);`

Формирует текстовое представление объекта, записывая его в файл `filename_`.
Возвращает ненулевое значение в случае успеха и ноль — в случае ошибки.

#### `uint8_t minsonPrettySerializeFile(t_minson_object *object_, char const *filename_);`

Формирует текстовое представление объекта с отступами и переносами строк,
записывая его в файл `filename_`. Возвращает ненулевое значение в случае
успеха и ноль — в случае ошибки.

#### `uint8_t minsonSerializeStream(t_minson_object *object_, FILE *stream_);`

Формирует текстовое представление объекта, записывая его в файловый поток.
Возвращает ненулевое значение в случае успеха и ноль — в случае ошибки.

#### `uint8_t minsonPrettySerializeStream(t_minson_object *object_, FILE *stream_);`

Формирует текстовое представление объекта с отступами и переносами строк,
записывая его в файловый поток. Возвращает ненулевое значение в случае успеха
и ноль — в случае ошибки.

--------------------------------

### Разбор текстового представления

#### `_Thread_local int minson_error_code;`

Тип ошибки, возникшей при разборе. Одна из констант:

- `MINSON_ERROR_OK` — разбор выполнен без ошибок;
- `MINSON_ERROR_UNKNOWN_TOKEN` — во время разбора встретилась неизвестная
  лексема;
- `MINSON_ERROR_WRONG_VALUE` — во время разбора не удалось разобрать значение
  объекта;
- `MINSON_ERROR_WRONG_SYNTAX` — во время разбора встретилась неверная
  синтаксическая конструкция.

#### `_Thread_local int minson_error_line;`<br>`_Thread_local int minson_error_column;`

Номер строки и номер символа в строке во входных данных разбора, где была
обнаружена ошибка.

#### `t_minson_object *minsonParse(char const *str_);`

Разбирает MinSON-объект из текстового представления, содержащегося в
нуль-терминированной строке. Возвращает разобранный объект, который следует
удалять с помощью функции `minsonDelete`. В случае ошибки возвращает `NULL`.

#### `t_minson_object *minsonParse2(char const *str_begin_, char const *str_end_);`

Разбирает MinSON-объект из текстового представления, содержащегося в памяти от
`str_begin_` (включительно) до `str_end_` (не включая). Возвращает разобранный
объект, который следует удалять с помощью функции `minsonDelete`. В случае
ошибки возвращает `NULL`.

#### `t_minson_object *minsonParseFile(char const *filename_);`

Разбирает MinSON-объект, текстовое представление которого записано в файле
`filename_`. Возвращает разобранный объект, который следует удалять с помощью
функции `minsonDelete`. В случае ошибки возвращает `NULL`.

#### `t_minson_object *minsonParseStream(FILE *stream_);`

Разбирает MinSON-объект из текстового представления, содержащегося в файловом
потоке. Разбор продолжается до конца объекта. Возвращает разобранный объект,
который следует удалять с помощью функции `minsonDelete`, или `NULL` в случае
ошибки.
