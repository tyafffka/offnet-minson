Программный интерфейс библиотеки — C++
--------------------------------------

_Заголовочный файл: `minson.hpp`_

_Пространство имён: `minson`_

--------------------------------

#### `class null_pointer_exception;`

_Исключение_. Генерируется, когда в конструктор передан нулевой указатель, или
внутри экземпляра класса была попытка взаимодействия с таковым указателем.

#### `class cast_error;`

_Исключение_. Генерируется при попытке привести MinSON-объект к типу данных,
которому он не соответствует.

#### `class not_found_exception;`

_Исключение_. Генерируется при попытке доступа к несуществующему элементу
массива.

#### `class parse_error;`

_Исключение_. Генерируется при ошибке разбора.

- `int code;` — тип ошибки; одна из констант `MINSON_ERROR_...`.
- `int position;` — номер символа во входных данных разбора, где была
  обнаружена ошибка.

#### `class serialize_error;`

_Исключение_. Генерируется, если обнаружена ошибка при сериализации.

- `size_t stop_size;` — размер результата, на котором была обнаружена ошибка.

--------------------------------

### `class object;`

Класс MinSON-объекта.

#### `object(t_minson_object *cp);`

_Конструктор_. Копирует объект из C-структуры.

#### `object(object const &cp);`

_Конструктор копирования_.

#### `object(object &&mv);`

_Конструктор перемещения_.

#### `object &operator =(object const &cp);`

_Оператор присваивания копированием_.

#### `object &operator =(object &&mv);`

_Оператор присваивания перемещением_.

#### `static object wrap(t_minson_object *wrap);`

Создаёт экземпляр класса над объектом, не копируя его, но передавая владение
экземпляру.

#### `static object string(std::string const &value_, std::string const &name_="");`

Создаёт MinSON-объект строки.

#### `static object number(int64_t value_, std::string const &name_="");`

Создаёт MinSON-объект числа.

#### `static object floatNumber(double value_, std::string const &name_="");`

Создаёт MinSON-объект вещественного числа.

#### `static object boolean(bool value_, std::string const &name_="");`

Создаёт MinSON-объект с булевым значением.

#### `static object null(std::string const &name_="");`

Создаёт MinSON-объект с пустым значением.

#### `bool hasName() const;`

Проверяет, есть ли у объекта имя (идентификатор).

#### `std::string name() const;`

Возвращает имя объекта, если оно есть, или пустую строку.

#### `operator std::string() const;`

_Оператор приведения типа_. Получает строковое значение из объекта.

#### `operator int64_t() const;`

_Оператор приведения типа_. Получает целочисленное значение из объекта.

#### `operator double() const;`

_Оператор приведения типа_. Получает вещественное значение из объекта.

#### `operator bool() const;`

_Оператор приведения типа_. Получает булевое значение из объекта.

#### `bool isNull() const;`

Проверяет, что тип объекта — пустой.

--------------------------------

### `class iterator;`

Класс итератора по массиву.

#### `iterator(iterator const &cp);`

_Конструктор копирования_.

#### `static iterator end();`<br>`static iterator rend();`

Возвращают оконечный итератор, который не указывает ни на какой объект.

#### `iterator &operator ++();`

_Оператор инкремента_. Переходит к следующему объекту в массиве.

#### `iterator &operator --();`

_Оператор декремента_. Переходит к предыдущему объекту в массиве.

#### `bool hasName() const;`

Проверяет, есть ли у объекта, на который указывает итератор, имя.

#### `std::string name() const;`

Возвращает имя объекта, на который указывает итератор.

#### `bool isNull() const;`

Проверяет, что тип объекта, на который указывает итератор, — пустой.

#### `object operator *() const;`

_Оператор разыменования_. Возвращает объект, на который указывает итератор.

--------------------------------

### `class array;`

_Базовый класс: `object`_

Класс MinSON-массива.

#### `explicit array(std::string const &name_="");`

_Конструктор_. Создаёт новый объект массива, опционально обладающий именем.

#### `array(t_minson_array *cp);`

_Конструктор_. Копирует объект из C-структуры.

#### `array(array const &cp);`

_Конструктор копирования_.

#### `array(array &&mv);`

_Конструктор перемещения_.

#### `array(object const &cp_cast);`

_Конструктор приведения типа копированием_.

#### `array(object &&mv_cast);`

_Конструктор приведения типа перемещением_.

#### `array &operator =(array const &cp);`

_Оператор присваивания копированием_.

#### `array &operator =(array &&mv);`

_Оператор присваивания перемещением_.

#### `static array wrap(t_minson_array *wrap);`

Создаёт экземпляр класса из C-структуры, передавая владение экземпляру.

#### `array &put(object &&object_);`

Переместить объект в массив. Возвращает сам массив (для цепочек вызовов).

#### `array &putArray(std::string const &name_="");`

Создать вложенный массив внутри объекта.

#### `array &putString(std::string const &value_, std::string const &name_="");`

Создать и поместить в массив строковый объект.

#### `array &putNumber(int64_t value_, std::string const &name_="");`

Создать и поместить в массив целочисленный объект.

#### `array &putFloatNumber(double value_, std::string const &name_="");`

Создать и поместить в массив объект вещественного числа.

#### `array &putBoolean(bool value_, std::string const &name_="");`

Создать и поместить в массив объект с булевым значением.

#### `array &putNull(std::string const &name_="");`

Создать и поместить в массив объект с пустым значением.

#### `array &update(array const &from_);`

Обновляет/дополняет массив объектами из массива `from_`. Рекурсивно для
вложенных массивов.

#### `bool has(std::string const &name_) const;`

Проверяет, содержится ли в массиве объект с указанным именем.

#### `object get(std::string const &name_) const;`

Возвращает объект из массива с указанным именем.

#### `iterator find(std::string const &name_) const;`<br>`iterator rfind(std::string const &name_) const;`

Возвращают итератор, указывающий на элемент с указанным именем, или оконечный
итератор, если элемент не найден. Направление обхода итератора прямое и
обратное соответственно.

#### `object pop(std::string const &name_);`<br>`object pop(iterator const &it_);`

Извлекают объект из массива по имени или по итератору, передавая владение
объектом вызывающему.

#### `void erase(std::string const &name_);`<br>`void erase(iterator const &it_);`

Удаляют объект с указанным именем, или на который указывает итератор, из
массива.

#### `size_t count() const;`<br>`size_t namedCount() const;`<br>`size_t unnamedCount() const;`

Возвращают общее число, число именованных и безымянных элементов массива.

#### `iterator begin() const;`<br>`iterator end() const;`

Возвращают итератор, указывающий на первый элемент массива, и оконечный
итератор.

#### `iterator rbegin() const;`<br>`iterator rend() const;`

Возвращают итератор, указывающий на последний элемент массива, и оконечный
итератор. Направление обхода итератора — от конца к началу.

--------------------------------

### `class string_builder;`

Класс строкового буфера для формирования строки.

#### `string_builder();`

_Конструктор по умолчанию_.

#### `string_builder &move(char const *mv, size_t str_size_);`

Переместить отрезок `mv` размера `str_size_` в буфер, передав владение. Память,
используемая отрезком, должна быть выделена с помощью функции `malloc`.
Возвращает сам буфер (для цепочек вызовов).

#### `string_builder &put(std::string const &str_);`

Скопировать отрезок `str_` в строковый буфер.

#### `string_builder &putChar(char ch_);`

Поместить символ `ch_` в строковый буфер.

#### `string_builder &serialize(object const &object_);`

Записать текстовое представление объекта в строковый буфер.

#### `size_t size() const;`

Возвращает получаемый размер строки.

#### `std::string build() const;`

Возвращает сформированную строку.

--------------------------------

#### `object parse(std::string const &str_);`

Функция разбора MinSON-объекта из текстового представления, содержащегося в
`str_`.

#### `object parse(std::string::iterator const begin_, std::string::iterator const end_);`

Функция разбора MinSON-объекта из текстового представления, заключённого между
`begin_` и `end_`.

#### `object parseFile(std::string const &filename_);`

Разбирает MinSON-объект, текстовое представление которого записано в файле
`filename_`.

#### `std::string serialize(object const &object_, bool is_pretty_=false);`

Возвращает текстовое представление объекта (с отступами и переносами строк,
если `is_pretty_ == true`).

#### `bool serializeFile(object const &object_, std::string const &filename_, bool is_pretty_=false);`

Записывает текстовое представление объекта(с отступами и переносами строк,
если `is_pretty_ == true`) в файл `filename_`. Возвращает успешность операции.
