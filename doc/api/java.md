Программный интерфейс библиотеки — Java
---------------------------------------

_Пакет: `bred_soft.minson`_

--------------------------------

### `class MinSONObject;`

Класс MinSON-объекта.

#### `static class CastError;`

Исключение, генерируемое при попытке получить значение, тип которого не
соответствует типу MinSON-объекта.

#### `static class ParseError;`

Исключение, генерируемое при ошибке разбора.

#### `static class SerializeError;`

Исключение, генерируемое при ошибке сериализации.

#### `static class NotFoundException;`

Исключение, генерируемое при попытке доступа к несуществующему элементу
массива.

#### `MinSONObject copy();`

Возвращает копию MinSON-объекта со всеми данными.

#### `static MinSONObject newString(String name_, String value_);`

Создаёт MinSON-объект строки. `name_` может быть **null**, тогда объект будет
безымянным.

#### `static MinSONObject newNumber(String name_, long value_);`

Создаёт MinSON-объект числа. `name_` может быть **null**, тогда объект будет
безымянным.

#### `static MinSONObject newFloatNumber(String name_, double value_);`

Создаёт MinSON-объект вещественного числа. `name_` может быть **null**, тогда
объект будет безымянным.

#### `static MinSONObject newBoolean(String name_, boolean value_);`

Создаёт MinSON-объект с булевым значением. `name_` может быть **null**, тогда
объект будет безымянным.

#### `static MinSONObject newNull(String name_);`

Создаёт MinSON-объект с пустым значением. `name_` может быть **null**, тогда
объект будет безымянным.

#### `static MinSONObject parse(String str_);`

Разбирает MinSON-объект из текстового представления, содержащегося в `str_`.

#### `static MinSONObject parseFile(String filename_);`

Разбирает MinSON-объект, текстовое представление которого записано в файле
`filename_`.

#### `String name();`

Возвращает имя объекта, если оно есть, или **null**.

#### `String getString();`

Возвращает строковое значение из объекта.

#### `long getNumber();`

Возвращает целочисленное значение из объекта.

#### `double getFloatNumber();`

Возвращает вещественное значение из объекта.

#### `boolean getBoolean();`

Возвращает булевое значение из объекта.

#### `boolean isNull();`

Проверяет, что тип объекта — пустой.

#### `String serialize();`

Возвращает текстовое представление объекта.

#### `String prettySerialize();`

Возвращает текстовое представление объекта с отступами и переносами строк.

#### `boolean serializeFile(String filename_);`

Записывает текстовое представление объекта в файл `filename_`. Возвращает
успешность операции.

#### `boolean prettySerializeFile(String filename_);`

Записывает текстовое представление объекта с отступами и переносами строк в
файл `filename_`. Возвращает успешность операции.

--------------------------------

### `class MinSONIterator;`

Класс итератора по массиву.

#### `MinSONIterator copy();`

Возвращает копию итератора.

#### `void next();`

Переходит к следующему объекту в массиве.

#### `void prev();`

Переходит к предыдущему объекту в массиве.

#### `boolean equals(Object r_);`

Проверяет, что итераторы указывают на один и тот же объект.

#### `String name();`

Возвращает имя объекта, на который указывает итератор, если оно есть, или
**null**.

#### `MinSONObject get();`

Возвращает объект, на который указывает итератор.

--------------------------------

### `class MinSONArray;`

Класс MinSON-массива.

#### `MinSONArray();`

_Конструктор_. Создаёт новый объект массива без имени.

#### `MinSONArray(String name_);`

_Конструктор_. Создаёт новый объект массива c именем `name_`.

#### `MinSONArray put(MinSONObject object_);`

Переместить объект в массив. Возвращает сам массив (для цепочек вызовов).

#### `MinSONArray putArray(String name_);`

Создать вложенный массив внутри объекта. Если `name_` — **null**, массив
будет безымянным.

#### `MinSONArray putString(String name_, String value_);`

Создать и поместить в массив строковый объект.

#### `MinSONArray putNumber(String name_, long value_);`

Создать и поместить в массив целочисленный объект.

#### `MinSONArray putFloatNumber(String name_, double value_);`

Создать и поместить в массив объект вещественного числа.

#### `MinSONArray putBoolean(String name_, boolean value_);`

Создать и поместить в массив объект с булевым значением.

#### `MinSONArray putNull(String name_);`

Создать и поместить в массив объект с пустым значением.

#### `MinSONArray update(MinSONArray from_);`

Обновляет/дополняет массив объектами из массива `from_`. Рекурсивно для
вложенных массивов.

#### `boolean has(String name_);`

Проверяет, содержится ли в массиве объект с указанным именем.

#### `MinSONObject get(String name_);`

Возвращает объект из массива с указанным именем.

#### `MinSONIterator find(String name_);`<br>`MinSONIterator rfind(String name_);`

Возвращают итератор, указывающий на элемент с указанным именем, или оконечный
итератор, если элемент не найден. Направление обхода итератора прямое и
обратное соответственно.

#### `MinSONObject pop(String name_);`<br>`MinSONObject pop(MinSONIterator it_);`

Извлекают объект из массива по имени или по итератору, возвращая его.

#### `void erase(String name_);`<br>`void erase(MinSONIterator it_);`

Удаляют объект с указанным именем, или на который указывает итератор, из
массива.

#### `long count();`<br>`long namedCount();`<br>`long unnamedCount();`

Возвращают общее число, число именованных и безымянных элементов массива.

#### `MinSONIterator begin();`<br>`MinSONIterator end();`

Возвращают итератор, указывающий на первый элемент массива, и оконечный
итератор.

#### `MinSONIterator rbegin();`<br>`MinSONIterator rend();`

Возвращают итератор, указывающий на последний элемент массива, и оконечный
итератор. Направление обхода итератора — от конца к началу.
