#ifndef __TEST_H
#define __TEST_H

#include <stdio.h>


#define _FAIL_(msg) \
do { \
    fprintf(stderr, "TEST FAILED [%s:%d]: %s\n", \
            __FILE__, __LINE__, msg); \
    return 1; \
} while(0)

#define _TEST_(expr) \
do { \
    if(! (expr)) \
    { \
        fprintf(stderr, "TEST FAILED at [%s:%d] %s(): %s\n", \
                __FILE__, __LINE__, __func__, #expr); \
        return 1; \
    } \
} while(0)

#define _ENDTEST_ do { return 0; } while(0)

// callable must return `int`
#define _SUBTEST_(callable, ...) \
do { \
    if((callable)(__VA_ARGS__) != 0) return 1; \
} while(0)


#endif // __TEST_H
