#ifndef __MINSON_PARSE_H
#define __MINSON_PARSE_H

#include "minson_string_builder.h"
#include <stdio.h>

#define __PARSE_OK            0
#define __PARSE_UNKNOWN_TOKEN 2
#define __PARSE_WRONG_VALUE   3


enum _c_token
{
    _C_IDENTIFIER, _C_STRING, _C_BASIC,
    _C_ARRAY_BEGIN, _C_ARRAY_END,
    _C_END,
};


struct _base_context
{
    t_minson_string_builder *token_value;
    int (*_get_callback)(struct _base_context *);
    int __buffered_char;
    int at_line, at_column;
    enum _c_token token_type;
};


struct _t_string_context;

struct _t_string_context *_new_parse_str(char const *begin_, char const *end_);
void _delete_parse_str(struct _t_string_context *this_);


struct _t_stream_context;

struct _t_stream_context *_new_parse_stream(FILE *stream_);
void _delete_parse_stream(struct _t_stream_context *this_);


/// Get one token
int /*nix*/ _parse_next(struct _base_context *this_);


#endif // __MINSON_PARSE_H
