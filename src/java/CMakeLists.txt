#--------------------------------------------------------------#
#                         Java sources                         #
#--------------------------------------------------------------#

find_package(Java REQUIRED)
find_package(JNI REQUIRED)

include(UseJava)

set(CMAKE_JAVA_COMPILE_FLAGS -h "${PROJECT_SOURCE_DIR}/src/java/")
set(JAR_DESTINATION "share/java")

include_directories(${JNI_INCLUDE_DIRS})

#--------------------------------------------------------------#

add_jar(offnet-minson-j
    bred_soft/minson/MinSONObject.java
    bred_soft/minson/MinSONIterator.java
    bred_soft/minson/MinSONArray.java
    
    VERSION ${PROJECT_VERSION}
    OUTPUT_NAME "offnet-minson"
)

get_target_property(JAR_FILES offnet-minson-j INSTALL_FILES)
install(FILES ${JAR_FILES}
    DESTINATION "${JAR_DESTINATION}" COMPONENT "Java"
)

export_set(offnet_minson_JAR_FILES
    "${CMAKE_INSTALL_PREFIX}/${JAR_DESTINATION}/offnet-minson.jar"
)


add_library(offnet-minson-jn SHARED
    bred_soft_minson_MinSONObject.c
    bred_soft_minson_MinSONIterator.c
    bred_soft_minson_MinSONArray.c
)

target_link_libraries(offnet-minson-jn
    offnet-minson
    ${JNI_LIBRARIES}
)

common_install(offnet-minson-jn COMPONENT "Java")


add_custom_target(offnet-minson-java DEPENDS
    offnet-minson-j
    offnet-minson-jn
)
