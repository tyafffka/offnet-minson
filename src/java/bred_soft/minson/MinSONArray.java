package bred_soft.minson;



/* Класс MinSON-массива. */
public class MinSONArray extends MinSONObject
{
    private native void __array_init(String name_);
    
    // ---
    
    MinSONArray(int __) {   super(0);   }
    
    // ---
    
    public MinSONArray() {   super(0); __array_init(null);   }
    public MinSONArray(String name_) {   super(0); __array_init(name_);   }
    
    public native MinSONArray put(MinSONObject object_);
    public native MinSONArray putArray(String name_);
    public native MinSONArray putString(String name_, String value_);
    public native MinSONArray putNumber(String name_, long value_);
    public native MinSONArray putFloatNumber(String name_, double value_);
    public native MinSONArray putBoolean(String name_, boolean value_);
    public native MinSONArray putNull(String name_);
    
    public native MinSONArray update(MinSONArray from_);
    
    public native boolean has(String name_);
    public native MinSONObject get(String name_);
    public native MinSONIterator find(String name_);
    public native MinSONIterator rfind(String name_);
    public native MinSONObject pop(String name_);
    public native MinSONObject pop(MinSONIterator it_);
    public native void erase(String name_);
    public native void erase(MinSONIterator it_);
    
    public native long count();
    public native long namedCount();
    public native long unnamedCount();
    
    public native MinSONIterator begin();
    public native MinSONIterator end();
    public native MinSONIterator rbegin();
    public native MinSONIterator rend();
}
