package bred_soft.minson;



/* Класс итератора по массиву. */
public class MinSONIterator
{
    private MinSONArray __back_ref;
    private long __p;
    private boolean __is_ascending;
    
    // ---
    
    MinSONIterator() { /* HOLDER */ }
    
    // ---
    
    public MinSONIterator copy()
    {
        MinSONIterator r = new MinSONIterator();
        r.__p = __p; r.__is_ascending = __is_ascending;
        return r;
    }
    
    public native void next();
    public native void prev();
    
    public boolean equals(Object r_)
    {   return(r_ instanceof MinSONIterator && __p == ((MinSONIterator)r_).__p);   }
    
    public native String name() throws NullPointerException;
    public native MinSONObject get() throws NullPointerException;
}
