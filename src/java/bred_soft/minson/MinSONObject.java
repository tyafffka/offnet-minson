package bred_soft.minson;



/* Класс MinSON-объекта. */
public class MinSONObject
{
    static
    {
        System.loadLibrary("offnet-minson-jn");
    }
    
    private long __obj;
    private boolean __is_own;
    
    private native void __destruct();
    
    // ---
    
    MinSONObject(int __) { /* HOLDER */ }
    
    protected void finalize() throws Throwable
    {
        __destruct(); super.finalize();
    }
    
    // ---
    
    public static class CastError extends Error
    {
        CastError() {   super();   }
        CastError(String message) {   super(message);   }
    }
    public static class ParseError extends Error
    {
        ParseError() {   super();   }
        ParseError(String message) {   super(message);   }
    }
    public static class SerializeError extends Error
    {
        SerializeError() {   super();   }
        SerializeError(String message) {   super(message);   }
    }
    public static class NotFoundException extends Error
    {
        NotFoundException() {   super();   }
        NotFoundException(String message) {   super(message);   }
    }
    
    public native MinSONObject copy();
    
    public static native MinSONObject newString(String name_, String value_);
    public static native MinSONObject newNumber(String name_, long value_);
    public static native MinSONObject newFloatNumber(String name_, double value_);
    public static native MinSONObject newBoolean(String name_, boolean value_);
    public static native MinSONObject newNull(String name_);
    
    public static native MinSONObject parse(String str_) throws ParseError;
    public static native MinSONObject parseFile(String filename_) throws ParseError;
    
    public native String name();
    
    public native String getString() throws CastError;
    public native long getNumber() throws CastError;
    public native double getFloatNumber() throws CastError;
    public native boolean getBoolean() throws CastError;
    public native boolean isNull();
    
    public native String serialize() throws SerializeError;
    public native String prettySerialize() throws SerializeError;
    public native boolean serializeFile(String filename_);
    public native boolean prettySerializeFile(String filename_);
}
