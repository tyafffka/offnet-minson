#include "bred_soft_minson_MinSONArray.h"
#include <minson.h>
#include <stdbool.h>

extern jobject JNICALL MinSON_instance(JNIEnv *env_,
                                       t_minson_object *obj_, bool is_own_);
extern t_minson_object *JNICALL MinSON_obj(JNIEnv *env_, jobject this_);
extern bool JNICALL MinSON_obj_own(JNIEnv *env_, jobject this_);
extern void JNICALL MinSON_obj_reset_own(JNIEnv *env_, jobject this_);
extern jobject JNICALL MinSON_iter(JNIEnv *env_, jobject back_ref_,
                                   t_minson_iter p_, bool is_ascending_);
extern t_minson_iter JNICALL MinSON_iter_get(JNIEnv *env_, jobject this_);



JNIEXPORT void JNICALL MinSON_put(JNIEnv *env_, jobject array_,
                                  t_minson_object *obj_, bool is_own_)
{
    t_minson_array *array = minsonCastArray(MinSON_obj(env_, array_));
    if(array == NULL) return;
    
    minsonDelete(minsonArrayRemove( array, obj_->name ));
    
    if(! is_own_) obj_ = minsonDuplicate(obj_);
    minsonArrayAdd(array, obj_);
    return;
}

JNIEXPORT t_minson_object *JNICALL MinSON_get(JNIEnv *env_, jobject array_,
                                              char const *name_, bool is_remove_)
{
    t_minson_array *array = minsonCastArray(MinSON_obj(env_, array_));
    if(array == NULL) return NULL;
    
    t_minson_object *obj;
    if(is_remove_) obj = minsonArrayRemove(array, name_);
    else obj = minsonArrayGet(array, name_);
    
    return obj;
}

JNIEXPORT t_minson_object *JNICALL MinSON_iter_pop(JNIEnv *env_, jobject array_, jobject iter_)
{
    t_minson_iter it = MinSON_iter_get(env_, iter_);
    if(minsonNotEnd(it))
    {
        t_minson_array *array = minsonCastArray(MinSON_obj(env_, array_));
        if(array == NULL) return NULL;
        
        return minsonIterRemove(array, it);
    }
    else
    {   return NULL;   }
}

// --- *** ---


JNIEXPORT void JNICALL Java_bred_1soft_minson_MinSONArray__1_1array_1init
    (JNIEnv *env_, jobject this_, jstring name_)
{
    char const *name = (*env_)->GetStringUTFChars(env_, name_, NULL);
    
    t_minson_array *obj = minsonCreateArray(name);
    
    if(name != NULL) (*env_)->ReleaseStringUTFChars(env_, name_, name);
    
    jclass class__ = (*env_)->GetObjectClass(env_, this_);
    jfieldID obj_f__ = (*env_)->GetFieldID(env_, class__, "__obj", "J");
    jfieldID is_own_f__ = (*env_)->GetFieldID(env_, class__, "__is_own", "Z");
    
    (*env_)->SetLongField(env_, this_, obj_f__, (jlong)(intptr_t)obj);
    (*env_)->SetBooleanField(env_, this_, is_own_f__, true);
    return;
}

// ---

JNIEXPORT jobject JNICALL Java_bred_1soft_minson_MinSONArray_put
    (JNIEnv *env_, jobject this_, jobject object_)
{
    MinSON_put(env_, this_, MinSON_obj(env_, object_), MinSON_obj_own(env_, object_));
    MinSON_obj_reset_own(env_, object_);
    return this_;
}

JNIEXPORT jobject JNICALL Java_bred_1soft_minson_MinSONArray_putArray
    (JNIEnv *env_, jobject this_, jstring name_)
{
    char const *name = (*env_)->GetStringUTFChars(env_, name_, NULL);
    
    MinSON_put(env_, this_, minsonUpcast(minsonCreateArray(name)), true);
    
    if(name != NULL) (*env_)->ReleaseStringUTFChars(env_, name_, name);
    return this_;
}

JNIEXPORT jobject JNICALL Java_bred_1soft_minson_MinSONArray_putString
    (JNIEnv *env_, jobject this_, jstring name_, jstring value_)
{
    char const *name = (*env_)->GetStringUTFChars(env_, name_, NULL);
    char const *value = (*env_)->GetStringUTFChars(env_, value_, NULL);
    if(value == NULL)
    {
        if(name != NULL) (*env_)->ReleaseStringUTFChars(env_, name_, name);
        return NULL;
    }
    
    MinSON_put(env_, this_, minsonCreateString(name, value), true);
    
    if(name != NULL) (*env_)->ReleaseStringUTFChars(env_, name_, name);
    (*env_)->ReleaseStringUTFChars(env_, value_, value);
    return this_;
}

JNIEXPORT jobject JNICALL Java_bred_1soft_minson_MinSONArray_putNumber
    (JNIEnv *env_, jobject this_, jstring name_, jlong value_)
{
    char const *name = (*env_)->GetStringUTFChars(env_, name_, NULL);
    
    MinSON_put(env_, this_, minsonCreateNumber(name, (int64_t)value_), true);
    
    if(name != NULL) (*env_)->ReleaseStringUTFChars(env_, name_, name);
    return this_;
}

JNIEXPORT jobject JNICALL Java_bred_1soft_minson_MinSONArray_putFloatNumber
    (JNIEnv *env_, jobject this_, jstring name_, jdouble value_)
{
    char const *name = (*env_)->GetStringUTFChars(env_, name_, NULL);
    
    MinSON_put(env_, this_, minsonCreateFloatNumber(name, (double)value_), true);
    
    if(name != NULL) (*env_)->ReleaseStringUTFChars(env_, name_, name);
    return this_;
}

JNIEXPORT jobject JNICALL Java_bred_1soft_minson_MinSONArray_putBoolean
    (JNIEnv *env_, jobject this_, jstring name_, jboolean value_)
{
    char const *name = (*env_)->GetStringUTFChars(env_, name_, NULL);
    
    MinSON_put(env_, this_, minsonCreateBoolean(name, (uint8_t)value_), true);
    
    if(name != NULL) (*env_)->ReleaseStringUTFChars(env_, name_, name);
    return this_;
}

JNIEXPORT jobject JNICALL Java_bred_1soft_minson_MinSONArray_putNull
    (JNIEnv *env_, jobject this_, jstring name_)
{
    char const *name = (*env_)->GetStringUTFChars(env_, name_, NULL);
    
    MinSON_put(env_, this_, minsonCreateNull(name), true);
    
    if(name != NULL) (*env_)->ReleaseStringUTFChars(env_, name_, name);
    return this_;
}

// ---

JNIEXPORT jobject JNICALL Java_bred_1soft_minson_MinSONArray_update
    (JNIEnv *env_, jobject this_, jobject from_)
{
    t_minson_array *array = minsonCastArray(MinSON_obj(env_, this_));
    if(array == NULL) return NULL;
    
    t_minson_array *from = minsonCastArray(MinSON_obj(env_, from_));
    if(from == NULL) return this_;
    
    minsonArrayUpdate(array, from);
    return this_;
}

// ---

JNIEXPORT jboolean JNICALL Java_bred_1soft_minson_MinSONArray_has
    (JNIEnv *env_, jobject this_, jstring name_)
{
    char const *name = (*env_)->GetStringUTFChars(env_, name_, NULL);
    
    jboolean r = false;
    if(name != NULL)
    {
        r = (jboolean)(MinSON_get(env_, this_, name, false) != NULL);
        (*env_)->ReleaseStringUTFChars(env_, name_, name);
    }
    else
    {
        jclass exception = (*env_)->FindClass(env_, "java/lang/NullPointerException");
        (*env_)->ThrowNew(env_, exception, "null name");
    }
    return r;
}

JNIEXPORT jobject JNICALL Java_bred_1soft_minson_MinSONArray_get
    (JNIEnv *env_, jobject this_, jstring name_)
{
    char const *name = (*env_)->GetStringUTFChars(env_, name_, NULL);
    
    if(name != NULL)
    {
        t_minson_object *obj = MinSON_get(env_, this_, name, false);
        jobject instance = NULL;
        
        if(obj != NULL)
        {   instance = MinSON_instance(env_, obj, false);   }
        else
        {
            jclass exception = (*env_)->FindClass(env_, "bred_soft/minson/MinSONObject$NotFoundException");
            (*env_)->ThrowNew(env_, exception, name);
        }
        
        (*env_)->ReleaseStringUTFChars(env_, name_, name);
        return instance;
    }
    else
    {
        jclass exception = (*env_)->FindClass(env_, "java/lang/NullPointerException");
        (*env_)->ThrowNew(env_, exception, "null name");
        return NULL;
    }
}

JNIEXPORT jobject JNICALL Java_bred_1soft_minson_MinSONArray_find
    (JNIEnv *env_, jobject this_, jstring name_)
{
    t_minson_array *array = minsonCastArray(MinSON_obj(env_, this_));
    if(array == NULL) return NULL;
    
    char const *name = (*env_)->GetStringUTFChars(env_, name_, NULL);
    t_minson_iter r = minsonArrayGetIter(array, name);
    (*env_)->ReleaseStringUTFChars(env_, name_, name);
    
    return MinSON_iter(env_, this_, r, true);
}

JNIEXPORT jobject JNICALL Java_bred_1soft_minson_MinSONArray_rfind
    (JNIEnv *env_, jobject this_, jstring name_)
{
    t_minson_array *array = minsonCastArray(MinSON_obj(env_, this_));
    if(array == NULL) return NULL;
    
    char const *name = (*env_)->GetStringUTFChars(env_, name_, NULL);
    t_minson_iter r = minsonArrayGetIter(array, name);
    (*env_)->ReleaseStringUTFChars(env_, name_, name);
    
    return MinSON_iter(env_, this_, r, false);
}

JNIEXPORT jobject JNICALL Java_bred_1soft_minson_MinSONArray_pop__Ljava_lang_String_2
    (JNIEnv *env_, jobject this_, jstring name_)
{
    char const *name = (*env_)->GetStringUTFChars(env_, name_, NULL);
    
    if(name != NULL)
    {
        t_minson_object *obj = MinSON_get(env_, this_, name, true);
        jobject instance = NULL;
        
        if(obj != NULL)
        {   instance = MinSON_instance(env_, obj, true);   }
        else
        {
            jclass exception = (*env_)->FindClass(env_, "bred_soft/minson/MinSONObject$NotFoundException");
            (*env_)->ThrowNew(env_, exception, name);
        }
        
        (*env_)->ReleaseStringUTFChars(env_, name_, name);
        return instance;
    }
    else
    {
        jclass exception = (*env_)->FindClass(env_, "java/lang/NullPointerException");
        (*env_)->ThrowNew(env_, exception, "null name");
        return NULL;
    }
}

JNIEXPORT jobject JNICALL Java_bred_1soft_minson_MinSONArray_pop__Lbred_1soft_minson_MinSONIterator_2
    (JNIEnv *env_, jobject this_, jobject it_)
{
    t_minson_object *obj = MinSON_iter_pop(env_, this_, it_);
    if(obj != NULL)
    {
        return MinSON_instance(env_, obj, true);
    }
    else
    {
        jclass exception = (*env_)->FindClass(env_, "java/lang/NullPointerException");
        (*env_)->ThrowNew(env_, exception, "ending iterator");
        return NULL;
    }
}

JNIEXPORT void JNICALL Java_bred_1soft_minson_MinSONArray_erase__Ljava_lang_String_2
    (JNIEnv *env_, jobject this_, jstring name_)
{
    char const *name = (*env_)->GetStringUTFChars(env_, name_, NULL);
    
    if(name != NULL)
    {
        minsonDelete(MinSON_get(env_, this_, name, true));
        (*env_)->ReleaseStringUTFChars(env_, name_, name);
    }
    else
    {
        jclass exception = (*env_)->FindClass(env_, "java/lang/NullPointerException");
        (*env_)->ThrowNew(env_, exception, "null name");
    }
    return;
}

JNIEXPORT void JNICALL Java_bred_1soft_minson_MinSONArray_erase__Lbred_1soft_minson_MinSONIterator_2
    (JNIEnv *env_, jobject this_, jobject it_)
{
    minsonDelete(MinSON_iter_pop(env_, this_, it_));
    return;
}

// ---

JNIEXPORT jlong JNICALL Java_bred_1soft_minson_MinSONArray_count
    (JNIEnv *env_, jobject this_)
{
    t_minson_array *array = minsonCastArray(MinSON_obj(env_, this_));
    if(array == NULL) return 0;
    return minsonArrayCount(array);
}

JNIEXPORT jlong JNICALL Java_bred_1soft_minson_MinSONArray_namedCount
    (JNIEnv *env_, jobject this_)
{
    t_minson_array *array = minsonCastArray(MinSON_obj(env_, this_));
    if(array == NULL) return 0;
    return minsonArrayNamedCount(array);
}

JNIEXPORT jlong JNICALL Java_bred_1soft_minson_MinSONArray_unnamedCount
    (JNIEnv *env_, jobject this_)
{
    t_minson_array *array = minsonCastArray(MinSON_obj(env_, this_));
    if(array == NULL) return 0;
    return minsonArrayCount(array) - minsonArrayNamedCount(array);
}

// ---

JNIEXPORT jobject JNICALL Java_bred_1soft_minson_MinSONArray_begin
    (JNIEnv *env_, jobject this_)
{
    t_minson_array *array = minsonCastArray(MinSON_obj(env_, this_));
    if(array == NULL) return NULL;
    return MinSON_iter(env_, this_, minsonFirst(array), true);
}

JNIEXPORT jobject JNICALL Java_bred_1soft_minson_MinSONArray_end
    (JNIEnv *env_, jobject this_)
{
    return MinSON_iter(env_, this_, NULL, true);
}

JNIEXPORT jobject JNICALL Java_bred_1soft_minson_MinSONArray_rbegin
    (JNIEnv *env_, jobject this_)
{
    t_minson_array *array = minsonCastArray(MinSON_obj(env_, this_));
    if(array == NULL) return NULL;
    return MinSON_iter(env_, this_, minsonLast(array), false);
}

JNIEXPORT jobject JNICALL Java_bred_1soft_minson_MinSONArray_rend
    (JNIEnv *env_, jobject this_)
{
    return MinSON_iter(env_, this_, NULL, false);
}
