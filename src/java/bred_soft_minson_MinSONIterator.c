#include "bred_soft_minson_MinSONIterator.h"
#include <minson.h>
#include <stdbool.h>

extern jobject JNICALL MinSON_instance(JNIEnv *env_,
                                       t_minson_object *obj_, bool is_own_);


JNIEXPORT jobject JNICALL MinSON_iter(JNIEnv *env_, jobject back_ref_,
                                      t_minson_iter p_, bool is_ascending_)
{
    jclass class__ = (*env_)->FindClass(env_, "bred_soft/minson/MinSONIterator");
    jmethodID init_f__ = (*env_)->GetMethodID(env_, class__, "<init>", "()V");
    jfieldID back_ref_f__ = (*env_)->GetFieldID(env_, class__, "__back_ref", "Lbred_soft/minson/MinSONArray;");
    jfieldID p_f__ = (*env_)->GetFieldID(env_, class__, "__p", "J");
    jfieldID is_ascending_f__ = (*env_)->GetFieldID(env_, class__, "__is_ascending", "Z");
    
    jobject r = (*env_)->NewObject(env_, class__, init_f__);
    (*env_)->SetObjectField(env_, r, back_ref_f__, back_ref_);
    (*env_)->SetLongField(env_, r, p_f__, (jlong)(intptr_t)p_);
    (*env_)->SetBooleanField(env_, r, is_ascending_f__, (jboolean)is_ascending_);
    return r;
}

JNIEXPORT t_minson_iter JNICALL MinSON_iter_get(JNIEnv *env_, jobject this_)
{
    jclass class__ = (*env_)->GetObjectClass(env_, this_);
    jfieldID p_f__ = (*env_)->GetFieldID(env_, class__, "__p", "J");
    return (t_minson_iter)(intptr_t)(*env_)->GetLongField(env_, this_, p_f__);
}

JNIEXPORT bool JNICALL MinSON_iter_ascending(JNIEnv *env_, jobject this_)
{
    jclass class__ = (*env_)->GetObjectClass(env_, this_);
    jfieldID is_ascending_f__ = (*env_)->GetFieldID(env_, class__, "__is_ascending", "Z");
    return (bool)(*env_)->GetBooleanField(env_, this_, is_ascending_f__);
}

JNIEXPORT void JNICALL MinSON_iter_set(JNIEnv *env_, jobject this_,
                                       t_minson_iter p_)
{
    jclass class__ = (*env_)->GetObjectClass(env_, this_);
    jfieldID p_f__ = (*env_)->GetFieldID(env_, class__, "__p", "J");
    
    (*env_)->SetLongField(env_, this_, p_f__, (jlong)(intptr_t)p_);
    return;
}

// --- *** ---


JNIEXPORT void JNICALL Java_bred_1soft_minson_MinSONIterator_next
    (JNIEnv *env_, jobject this_)
{
    t_minson_iter p = MinSON_iter_get(env_, this_);
    if(minsonNotEnd(p))
    {
        if(MinSON_iter_ascending(env_, this_))
        {   minsonNext(p);   }
        else
        {   minsonPrev(p);   }
        
        MinSON_iter_set(env_, this_, p);
    }
    return;
}

JNIEXPORT void JNICALL Java_bred_1soft_minson_MinSONIterator_prev
    (JNIEnv *env_, jobject this_)
{
    t_minson_iter p = MinSON_iter_get(env_, this_);
    if(minsonNotEnd(p))
    {
        if(MinSON_iter_ascending(env_, this_))
        {   minsonPrev(p);   }
        else
        {    minsonNext(p);   }
        
        MinSON_iter_set(env_, this_, p);
    }
    return;
}

// ---

JNIEXPORT jstring JNICALL Java_bred_1soft_minson_MinSONIterator_name
    (JNIEnv *env_, jobject this_)
{
    t_minson_iter p = MinSON_iter_get(env_, this_);
    if(minsonNotEnd(p))
    {
        if(minsonIterName(p) == NULL) return NULL;
        return (*env_)->NewStringUTF(env_, minsonIterName(p));
    }
    else
    {
        jclass exception = (*env_)->FindClass(env_, "java/lang/NullPointerException");
        (*env_)->ThrowNew(env_, exception, "ending iterator");
        return NULL;
    }
}

JNIEXPORT jobject JNICALL Java_bred_1soft_minson_MinSONIterator_get
    (JNIEnv *env_, jobject this_)
{
    t_minson_iter p = MinSON_iter_get(env_, this_);
    if(minsonNotEnd(p))
    {
        return MinSON_instance(env_, minsonIterObject(p), false);
    }
    else
    {
        jclass exception = (*env_)->FindClass(env_, "java/lang/NullPointerException");
        (*env_)->ThrowNew(env_, exception, "ending iterator");
        return NULL;
    }
}
