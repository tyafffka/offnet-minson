#include "bred_soft_minson_MinSONObject.h"
#include <minson.h>
#include <stdbool.h>



JNIEXPORT jobject JNICALL MinSON_instance(JNIEnv *env_,
                                          t_minson_object *obj_, bool is_own_)
{
    jclass class__;
    if(obj_->type == C_MINSON_ARRAY)
        class__ = (*env_)->FindClass(env_, "bred_soft/minson/MinSONArray");
    else
        class__ = (*env_)->FindClass(env_, "bred_soft/minson/MinSONObject");
    
    jmethodID init_f__ = (*env_)->GetMethodID(env_, class__, "<init>", "(I)V");
    jfieldID obj_f__ = (*env_)->GetFieldID(env_, class__, "__obj", "J");
    jfieldID is_own_f__ = (*env_)->GetFieldID(env_, class__, "__is_own", "Z");
    
    jobject r = (*env_)->NewObject(env_, class__, init_f__);
    (*env_)->SetLongField(env_, r, obj_f__, (jlong)(intptr_t)obj_);
    (*env_)->SetBooleanField(env_, r, is_own_f__, (jboolean)is_own_);
    return r;
}

JNIEXPORT t_minson_object *JNICALL MinSON_obj(JNIEnv *env_, jobject this_)
{
    jclass class__ = (*env_)->GetObjectClass(env_, this_);
    jfieldID obj_f__ = (*env_)->GetFieldID(env_, class__, "__obj", "J");
    
    return (t_minson_object *)(intptr_t)(*env_)->GetLongField(env_, this_, obj_f__);
}

JNIEXPORT bool JNICALL MinSON_obj_own(JNIEnv *env_, jobject this_)
{
    jclass class__ = (*env_)->GetObjectClass(env_, this_);
    jfieldID is_own_f__ = (*env_)->GetFieldID(env_, class__, "__is_own", "Z");
    
    return (bool)(*env_)->GetBooleanField(env_, this_, is_own_f__);
}

JNIEXPORT void JNICALL MinSON_obj_reset_own(JNIEnv *env_, jobject this_)
{
    jclass class__ = (*env_)->GetObjectClass(env_, this_);
    jfieldID is_own_f__ = (*env_)->GetFieldID(env_, class__, "__is_own", "Z");
    
    (*env_)->SetBooleanField(env_, this_, is_own_f__, false);
    return;
}

// --- *** ---


JNIEXPORT void JNICALL Java_bred_1soft_minson_MinSONObject__1_1destruct
    (JNIEnv *env_, jobject this_)
{
    jclass class__ = (*env_)->GetObjectClass(env_, this_);
    jfieldID obj_f__ = (*env_)->GetFieldID(env_, class__, "__obj", "J");
    jfieldID is_own_f__ = (*env_)->GetFieldID(env_, class__, "__is_own", "Z");
    
    if((*env_)->GetBooleanField(env_, this_, is_own_f__))
    {
        minsonDelete(
            (t_minson_object *)(intptr_t)(*env_)->GetLongField(env_, this_, obj_f__)
        );
    }
    return;
}

// ---

JNIEXPORT jobject JNICALL Java_bred_1soft_minson_MinSONObject_copy
    (JNIEnv *env_, jobject this_)
{
    t_minson_object *obj = MinSON_obj(env_, this_);
    return MinSON_instance(env_, minsonDuplicate(obj), true);
}

// ---

JNIEXPORT jobject JNICALL Java_bred_1soft_minson_MinSONObject_newString
    (JNIEnv *env_, jclass class_, jstring name_, jstring value_)
{
    char const *name = (*env_)->GetStringUTFChars(env_, name_, NULL);
    char const *value = (*env_)->GetStringUTFChars(env_, value_, NULL);
    if(value == NULL)
    {
        if(name != NULL) (*env_)->ReleaseStringUTFChars(env_, name_, name);
        return NULL;
    }
    
    t_minson_object *obj = minsonCreateString(name, value);
    
    if(name != NULL) (*env_)->ReleaseStringUTFChars(env_, name_, name);
    (*env_)->ReleaseStringUTFChars(env_, value_, value);
    return MinSON_instance(env_, obj, true);
}

JNIEXPORT jobject JNICALL Java_bred_1soft_minson_MinSONObject_newNumber
    (JNIEnv *env_, jclass class_, jstring name_, jlong value_)
{
    char const *name = (*env_)->GetStringUTFChars(env_, name_, NULL);
    
    t_minson_object *obj = minsonCreateNumber(name, (int64_t)value_);
    
    if(name != NULL) (*env_)->ReleaseStringUTFChars(env_, name_, name);
    return MinSON_instance(env_, obj, true);
}

JNIEXPORT jobject JNICALL Java_bred_1soft_minson_MinSONObject_newFloatNumber
    (JNIEnv *env_, jclass class_, jstring name_, jdouble value_)
{
    char const *name = (*env_)->GetStringUTFChars(env_, name_, NULL);
    
    t_minson_object *obj = minsonCreateFloatNumber(name, (double)value_);
    
    if(name != NULL) (*env_)->ReleaseStringUTFChars(env_, name_, name);
    return MinSON_instance(env_, obj, true);
}

JNIEXPORT jobject JNICALL Java_bred_1soft_minson_MinSONObject_newBoolean
    (JNIEnv *env_, jclass class_, jstring name_, jboolean value_)
{
    char const *name = (*env_)->GetStringUTFChars(env_, name_, NULL);
    
    t_minson_object *obj = minsonCreateBoolean(name, (uint8_t)value_);
    
    if(name != NULL) (*env_)->ReleaseStringUTFChars(env_, name_, name);
    return MinSON_instance(env_, obj, true);
}

JNIEXPORT jobject JNICALL Java_bred_1soft_minson_MinSONObject_newNull
    (JNIEnv *env_, jclass class_, jstring name_)
{
    char const *name = (*env_)->GetStringUTFChars(env_, name_, NULL);
    
    t_minson_object *obj = minsonCreateNull(name);
    
    if(name != NULL) (*env_)->ReleaseStringUTFChars(env_, name_, name);
    return MinSON_instance(env_, obj, true);
}

// ---

JNIEXPORT jobject JNICALL Java_bred_1soft_minson_MinSONObject_parse
    (JNIEnv *env_, jclass class_, jstring str_)
{
    char const *str = (*env_)->GetStringUTFChars(env_, str_, NULL);
    if(str == NULL) return NULL;
    
    t_minson_object *obj = minsonParse(str);
    
    (*env_)->ReleaseStringUTFChars(env_, str_, str);
    
    if(obj == NULL)
    {
        jclass exception = (*env_)->FindClass(env_, "bred_soft/minson/MinSONObject$ParseError");
        char buff[256];
        snprintf(buff, sizeof(buff), "Parsing of string failed at line %d, column %d!",
                 minson_error_line, minson_error_column);
        (*env_)->ThrowNew(env_, exception, buff);
        return NULL;
    }
    return MinSON_instance(env_, obj, true);
}

JNIEXPORT jobject JNICALL Java_bred_1soft_minson_MinSONObject_parseFile
    (JNIEnv *env_, jclass class_, jstring filename_)
{
    char const *filename = (*env_)->GetStringUTFChars(env_, filename_, NULL);
    if(filename == NULL) return NULL;
    
    t_minson_object *obj = minsonParseFile(filename);
    
    (*env_)->ReleaseStringUTFChars(env_, filename_, filename);
    
    if(obj == NULL)
    {
        jclass exception = (*env_)->FindClass(env_, "bred_soft/minson/MinSONObject$ParseError");
        char buff[256];
        snprintf(buff, sizeof(buff), "Parsing of file failed at line %d, column %d!",
                 minson_error_line, minson_error_column);
        (*env_)->ThrowNew(env_, exception, buff);
        return NULL;
    }
    return MinSON_instance(env_, obj, true);
}

// ---

JNIEXPORT jstring JNICALL Java_bred_1soft_minson_MinSONObject_name
    (JNIEnv *env_, jobject this_)
{
    t_minson_object *obj = MinSON_obj(env_, this_);
    if(obj->name == NULL) return NULL;
    return (*env_)->NewStringUTF(env_, obj->name);
}

// ---

JNIEXPORT jstring JNICALL Java_bred_1soft_minson_MinSONObject_getString
    (JNIEnv *env_, jobject this_)
{
    t_minson_object *obj = MinSON_obj(env_, this_);
    char const *value = minsonGetString(obj);
    
    if(value == NULL)
    {
        jclass exception = (*env_)->FindClass(env_, "bred_soft/minson/MinSONObject$CastError");
        (*env_)->ThrowNew(env_, exception, "");
        return NULL;
    }
    return (*env_)->NewStringUTF(env_, value);
}

JNIEXPORT jlong JNICALL Java_bred_1soft_minson_MinSONObject_getNumber
    (JNIEnv *env_, jobject this_)
{
    t_minson_object *obj = MinSON_obj(env_, this_);
    int64_t value;
    
    if(! minsonGetNumber(obj, &value))
    {
        jclass exception = (*env_)->FindClass(env_, "bred_soft/minson/MinSONObject$CastError");
        (*env_)->ThrowNew(env_, exception, "");
        return 0;
    }
    return (jlong)value;
}

JNIEXPORT jdouble JNICALL Java_bred_1soft_minson_MinSONObject_getFloatNumber
    (JNIEnv *env_, jobject this_)
{
    t_minson_object *obj = MinSON_obj(env_, this_);
    double value;
    
    if(! minsonGetFloatNumber(obj, &value))
    {
        jclass exception = (*env_)->FindClass(env_, "bred_soft/minson/MinSONObject$CastError");
        (*env_)->ThrowNew(env_, exception, "");
        return .0;
    }
    return (jdouble)value;
}

JNIEXPORT jboolean JNICALL Java_bred_1soft_minson_MinSONObject_getBoolean
    (JNIEnv *env_, jobject this_)
{
    t_minson_object *obj = MinSON_obj(env_, this_);
    uint8_t value;
    
    if(! minsonGetBoolean(obj, &value))
    {
        jclass exception = (*env_)->FindClass(env_, "bred_soft/minson/MinSONObject$CastError");
        (*env_)->ThrowNew(env_, exception, "");
        return 0;
    }
    return (jboolean)value;
}

JNIEXPORT jboolean JNICALL Java_bred_1soft_minson_MinSONObject_isNull
    (JNIEnv *env_, jobject this_)
{
    t_minson_object *obj = MinSON_obj(env_, this_);
    return (jboolean)minsonIsNull(obj);
}

// ---

JNIEXPORT jstring JNICALL Java_bred_1soft_minson_MinSONObject_serialize
    (JNIEnv *env_, jobject this_)
{
    t_minson_object *obj = MinSON_obj(env_, this_);
    t_minson_string_builder *builder = minsonCreateStringBuilder();
    
    if(! minsonSerialize(obj, builder))
    {
        jclass exception = (*env_)->FindClass(env_, "bred_soft/minson/MinSONObject$SerializeError");
        (*env_)->ThrowNew(env_, exception, "");
        minsonDeleteStringBuilder(builder);
        return NULL;
    }
    
    size_t size = minsonStringBuilderSize(builder);
    char buffer[size + 1];
    minsonBuildString(builder, buffer);
    buffer[size] = '\0';
    minsonDeleteStringBuilder(builder);
    
    return (*env_)->NewStringUTF(env_, buffer);
}

JNIEXPORT jstring JNICALL Java_bred_1soft_minson_MinSONObject_prettySerialize
    (JNIEnv *env_, jobject this_)
{
    t_minson_object *obj = MinSON_obj(env_, this_);
    t_minson_string_builder *builder = minsonCreateStringBuilder();
    
    if(! minsonPrettySerialize(obj, builder))
    {
        jclass exception = (*env_)->FindClass(env_, "bred_soft/minson/MinSONObject$SerializeError");
        (*env_)->ThrowNew(env_, exception, "");
        minsonDeleteStringBuilder(builder);
        return NULL;
    }
    
    size_t size = minsonStringBuilderSize(builder);
    char buffer[size + 1];
    minsonBuildString(builder, buffer);
    buffer[size] = '\0';
    minsonDeleteStringBuilder(builder);
    
    return (*env_)->NewStringUTF(env_, buffer);
}

JNIEXPORT jboolean JNICALL Java_bred_1soft_minson_MinSONObject_serializeFile
    (JNIEnv *env_, jobject this_, jstring filename_)
{
    char const *filename = (*env_)->GetStringUTFChars(env_, filename_, NULL);
    if(filename == NULL) return 0;
    
    t_minson_object *obj = MinSON_obj(env_, this_);
    jboolean r = minsonSerializeFile(obj, filename);
    
    (*env_)->ReleaseStringUTFChars(env_, filename_, filename);
    return r;
}

JNIEXPORT jboolean JNICALL Java_bred_1soft_minson_MinSONObject_prettySerializeFile
    (JNIEnv *env_, jobject this_, jstring filename_)
{
    char const *filename = (*env_)->GetStringUTFChars(env_, filename_, NULL);
    if(filename == NULL) return 0;
    
    t_minson_object *obj = MinSON_obj(env_, this_);
    jboolean r = minsonPrettySerializeFile(obj, filename);
    
    (*env_)->ReleaseStringUTFChars(env_, filename_, filename);
    return r;
}
