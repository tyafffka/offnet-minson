#ifndef _OFFNET_MINSON_HPP
#define _OFFNET_MINSON_HPP

#include "minson_object.hpp"
#include "minson_array.hpp"
#include "minson_string_builder.hpp"

namespace minson
{


/* Функция разбора MinSON-объекта из текстового представления, содержащегося в `str_`. */
inline object parse(std::string const &str_) throw(parse_error)
{
    register t_minson_object *obj__ = minsonParse(str_.c_str());
    if(! obj__) throw parse_error{minson_error_code, minson_error_line, minson_error_column};
    return object::wrap(obj__);
}

/* Функция разбора MinSON-объекта из текстового представления,
 * заключённого между `begin_` и `end_`. */
inline object parse(std::string::iterator const begin_,
                    std::string::iterator const end_) throw(parse_error)
{
    register t_minson_object *obj__ = minsonParse2(begin_.base(), end_.base());
    if(! obj__) throw parse_error{minson_error_code, minson_error_line, minson_error_column};
    return object::wrap(obj__);
}

/* Разбирает MinSON-объект, текстовое представление которого записано в файле `filename_`. */
inline object parseFile(std::string const &filename_) throw(parse_error)
{
    register t_minson_object *obj__ = minsonParseFile(filename_.c_str());
    if(! obj__) throw parse_error{minson_error_code, minson_error_line, minson_error_column};
    return object::wrap(obj__);
}


/* Возвращает текстовое представление объекта. */
inline std::string serialize(object const &object_, bool is_pretty_=false)
{
    string_builder builder;
    return builder.serialize(object_, is_pretty_).build();
}

/* Записывает текстовое представление объекта в файл `filename_`. */
inline bool serializeFile(object const &object_, std::string const &filename_,
                          bool is_pretty_=false)
{
    if(is_pretty_)
        return minsonPrettySerializeFile(object_.__get_obj(), filename_.c_str());
    else
        return minsonSerializeFile(object_.__get_obj(), filename_.c_str());
}


} // namespace minson

#endif // _OFFNET_MINSON_HPP
