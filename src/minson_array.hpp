#ifndef _OFFNET_MINSON_ARRAY_HPP
#define _OFFNET_MINSON_ARRAY_HPP

#include "minson_object.hpp"
#include "minson_iterator.hpp"

namespace minson
{


/* Класс MinSON-массива. */
class array : public object
{
private:
    inline t_minson_array *__a() const
    {   return reinterpret_cast<t_minson_array *>(__obj);   }
    
public:
    explicit array(std::string const &name_="") :
        object(minsonUpcast(minsonCreateArray( name_.c_str() )), true)
    {}
    array(t_minson_array *cp) :
        object(minsonDuplicate(minsonUpcast(cp)), true)
    {}
    array(array const &cp) :
        object(cp)
    {}
    array(array &&mv) :
        object(static_cast<object &&>(mv))
    {}
    array(object const &cp_cast) throw(cast_error) :
        object(cp_cast)
    {   if(__obj->type != C_MINSON_ARRAY) throw cast_error();   }
    array(object &&mv_cast) throw(cast_error) :
        object(static_cast<object &&>(mv_cast))
    {   if(__obj->type != C_MINSON_ARRAY) throw cast_error();   }
    
    array &operator =(array const &cp)
    {
        object::operator =(cp); return *this;
    }
    array &operator =(array &&mv)
    {
        object::operator =(static_cast<object &&>(mv)); return *this;
    }
    
    static array wrap(t_minson_array *wrap)
    {   return array(static_cast<object &&>( object(minsonUpcast(wrap), true) ));   }
    
    array &put(object &&object_)
    {
        register t_minson_object *obj__ = object_.__obj;
        if(! object_.__is_own) obj__ = minsonDuplicate(obj__);
        object_.__is_own = false;
        minsonDelete(minsonArrayRemove( __a(), obj__->name ));
        minsonArrayAdd(__a(), obj__);
        return *this;
    }
    array &putArray(std::string const &name_="")
    {
        put(array(name_)); return *this;
    }
    array &putString(std::string const &value_, std::string const &name_="")
    {
        put(object::string(value_, name_)); return *this;
    }
    array &putNumber(int64_t value_, std::string const &name_="")
    {
        put(object::number(value_, name_)); return *this;
    }
    array &putFloatNumber(double value_, std::string const &name_="")
    {
        put(object::floatNumber(value_, name_)); return *this;
    }
    array &putBoolean(bool value_, std::string const &name_="")
    {
        put(object::boolean(value_, name_)); return *this;
    }
    array &putNull(std::string const &name_="")
    {
        put(object::null(name_)); return *this;
    }
    
    array &update(array const &from_)
    {   minsonArrayUpdate(__a(), from_.__a()); return *this;   }
    
    bool has(std::string const &name_) const
    {   return (bool)minsonArrayGetIter(__a(), name_.c_str());   }
    
    object get(std::string const &name_) const throw(not_found_exception)
    {
        register t_minson_object *obj__ = minsonArrayGet(__a(), name_.c_str());
        if(! obj__) throw not_found_exception();
        return object(obj__, false);
    }
    iterator find(std::string const &name_) const
    {
        t_minson_iter p__ = minsonArrayGetIter(__a(), name_.c_str());
        return iterator(p__, true);
    }
    iterator rfind(std::string const &name_) const
    {
        t_minson_iter p__ = minsonArrayGetIter(__a(), name_.c_str());
        return iterator(p__, false);
    }
    
    object pop(std::string const &name_) throw(not_found_exception)
    {
        register t_minson_object *obj__ = minsonArrayRemove(__a(), name_.c_str());
        if(! obj__) throw not_found_exception();
        return object(obj__, true);
    }
    object pop(iterator const &it_) throw(null_pointer_exception)
    {
        if(! it_.__p) throw null_pointer_exception();
        register t_minson_object *obj__ = minsonIterRemove(__a(), it_.__p);
        return object(obj__, true);
    }
    
    void erase(std::string const &name_)
    {   minsonDelete(minsonArrayRemove( __a(), name_.c_str() ));   }
    
    void erase(iterator const &it_)
    {   minsonDelete(minsonIterRemove( __a(), it_.__p ));   }
    
    size_t count() const
    {   return minsonArrayCount(__a());   }
    
    size_t namedCount() const
    {   return minsonArrayNamedCount(__a());   }
    
    size_t unnamedCount() const
    {   return minsonArrayCount(__a()) - minsonArrayNamedCount(__a());   }
    
    iterator begin() const
    {   return iterator(minsonFirst(__a()), true);   }
    
    iterator end() const
    {   return iterator::end();   }
    
    iterator rbegin() const
    {   return iterator(minsonLast(__a()), false);   }
    
    iterator rend() const
    {   return iterator::rend();   }
}; // class array


} // namespace minson

#endif // _OFFNET_MINSON_ARRAY_HPP
