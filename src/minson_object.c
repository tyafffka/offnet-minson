#include "m.h"
#include "minson_object.h"
#include "minson_array.h"


t_minson_object *minsonCreateString(char const *name_, char const *value_)
{
    if(value_ == NULL) return NULL;
    if(name_ != NULL) if(name_[0] == '\0') name_ = NULL;
    
    t_minson_string *string = m_new(t_minson_string, 1);
    
    string->h.type = C_MINSON_STRING;
    string->h.name = m_strdup(name_);
    string->value = m_strdup(value_);
    return m_pcast(t_minson_object *, string);
}

t_minson_object *minsonCreateNumber(char const *name_, int64_t value_)
{
    if(name_ != NULL) if(name_[0] == '\0') name_ = NULL;
    
    t_minson_number *number = m_new(t_minson_number, 1);
    
    number->h.type = C_MINSON_NUMBER;
    number->h.name = m_strdup(name_);
    number->value = value_;
    return m_pcast(t_minson_object *, number);
}

t_minson_object *minsonCreateFloatNumber(char const *name_, double value_)
{
    if(name_ != NULL) if(name_[0] == '\0') name_ = NULL;
    
    t_minson_float_number *float_number = m_new(t_minson_float_number, 1);
    
    float_number->h.type = C_MINSON_FLOAT_NUMBER;
    float_number->h.name = m_strdup(name_);
    float_number->value = value_;
    return m_pcast(t_minson_object *, float_number);
}

t_minson_object *minsonCreateBoolean(char const *name_, uint8_t value_)
{
    if(name_ != NULL) if(name_[0] == '\0') name_ = NULL;
    
    t_minson_boolean *boolean = m_new(t_minson_boolean, 1);
    
    boolean->h.type = C_MINSON_BOOLEAN;
    boolean->h.name = m_strdup(name_);
    boolean->value = value_;
    return m_pcast(t_minson_object *, boolean);
}

t_minson_object *minsonCreateNull(char const *name_)
{
    if(name_ != NULL) if(name_[0] == '\0') name_ = NULL;
    
    t_minson_object *object = m_new(t_minson_object, 1);
    
    object->type = C_MINSON_NULL;
    object->name = m_strdup(name_);
    return object;
}

// --- *** ---


char const *minsonGetString(t_minson_object *object_)
{
    if(object_ == NULL) return NULL;
    if(object_->type != C_MINSON_STRING) return NULL;
    return m_pcast(t_minson_string *, object_)->value;
}

uint8_t /*bool*/ minsonGetNumber(t_minson_object *object_, int64_t *out_value_)
{
    if(object_ == NULL) return 0;
    if(object_->type != C_MINSON_NUMBER) return 0;
    
    *out_value_ = m_pcast(t_minson_number *, object_)->value;
    return 1;
}

uint8_t /*bool*/ minsonGetFloatNumber(t_minson_object *object_, double *out_value_)
{
    if(object_ == NULL) return 0;
    if(object_->type != C_MINSON_FLOAT_NUMBER) return 0;
    
    *out_value_ = m_pcast(t_minson_float_number *, object_)->value;
    return 1;
}

uint8_t /*bool*/ minsonGetBoolean(t_minson_object *object_, uint8_t /*bool*/ *out_value_)
{
    if(object_ == NULL) return 0;
    if(object_->type != C_MINSON_BOOLEAN) return 0;
    
    *out_value_ = m_pcast(t_minson_boolean *, object_)->value;
    return 1;
}

uint8_t /*bool*/ minsonIsNull(t_minson_object *object_)
{
    if(object_ == NULL) return 0;
    return (uint8_t)(object_->type == C_MINSON_NULL);
}

// --- *** ---


t_minson_object *minsonDuplicate(t_minson_object *object_)
{
    if(object_ == NULL) return NULL;
    
    switch(object_->type)
    {
    case C_MINSON_ARRAY:
    {
        t_minson_array *array = minsonCreateArray(object_->name);
        
        for(t_minson_iter it = minsonFirst(minsonCastArray(object_));
            minsonNotEnd(it); minsonNext(it))
        {
            minsonArrayAdd(array, minsonDuplicate(minsonIterObject(it)));
        }
        return minsonUpcast(array);
    }
    case C_MINSON_STRING:
        return minsonCreateString(
            object_->name, m_pcast(t_minson_string *, object_)->value
        );
    case C_MINSON_NUMBER:
        return minsonCreateNumber(
            object_->name, m_pcast(t_minson_number *, object_)->value
        );
    case C_MINSON_FLOAT_NUMBER:
        return minsonCreateFloatNumber(
            object_->name, m_pcast(t_minson_float_number *, object_)->value
        );
    case C_MINSON_BOOLEAN:
        return minsonCreateBoolean(
            object_->name, m_pcast(t_minson_boolean *, object_)->value
        );
    default:
        return minsonCreateNull(object_->name);
    }
}


void minsonDelete(struct t_minson_object *object_)
{
    if(object_ == NULL) return;
    
    switch(object_->type)
    {
    case C_MINSON_ARRAY:
    {
        t_minson_array *array = m_pcast(t_minson_array *, object_);
        
        for(t_minson_iter iter = minsonFirst(array); minsonNotEnd(iter); minsonNext(iter))
            minsonDelete(minsonIterObject(iter));
        
        minsonArrayClear(array);
        break;
    }
    case C_MINSON_STRING:
        m_delete(m_pcast(t_minson_string *, object_)->value);
        break;
    
    default:;
    }
    
    if(object_->name != NULL) m_delete(object_->name);
    
    m_delete(object_);
    return;
}
