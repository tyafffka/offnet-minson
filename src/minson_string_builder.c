#include "m.h"
#include "minson_string_builder.h"

#ifndef _MINSON_STRING_DATA_CAPASITY
# define _MINSON_STRING_DATA_CAPASITY 64
#endif


struct __t_minson_string_piece
{
    struct __t_minson_string_piece *next;
    char *buffer;
    size_t allocated, size;
};

struct t_minson_string_builder
{
    struct __t_minson_string_piece *__first, *__last;
};



t_minson_string_builder *minsonCreateStringBuilder()
{
    struct __t_minson_string_piece *piece = m_new(struct __t_minson_string_piece, 1);
    piece->next = NULL;
    piece->buffer = (char *)malloc(_MINSON_STRING_DATA_CAPASITY);
    piece->allocated = _MINSON_STRING_DATA_CAPASITY;
    piece->size = 0;
    
    t_minson_string_builder *this = m_new(t_minson_string_builder, 1);
    this->__first = this->__last = piece;
    return this;
}


void minsonDeleteStringBuilder(t_minson_string_builder *this_)
{
    struct __t_minson_string_piece *cur, *prev;
    for(cur = this_->__first; cur != NULL;)
    {
        prev = cur; cur = cur->next;
        free(prev->buffer);
        m_delete(prev);
    }
    
    m_delete(this_);
    return;
}


void minsonStringBuilderMove(t_minson_string_builder *this_,
                             char const *str_, size_t str_size_)
{
    struct __t_minson_string_piece *piece = m_new(struct __t_minson_string_piece, 1);
    piece->next = NULL;
    piece->buffer = m_pcast(char *, str_);
    piece->allocated = piece->size = str_size_;
    
    this_->__last->next = piece; this_->__last = piece;
    return;
}


void minsonStringBuilderPutChar(t_minson_string_builder *this_, char ch_)
{
    struct __t_minson_string_piece *piece;
    if(this_->__last->size < this_->__last->allocated)
    {
        piece = this_->__last;
    }
    else
    {
        piece = m_new(struct __t_minson_string_piece, 1);
        piece->next = NULL;
        piece->buffer = (char *)malloc(_MINSON_STRING_DATA_CAPASITY);
        piece->allocated = _MINSON_STRING_DATA_CAPASITY;
        piece->size = 0;
        
        this_->__last->next = piece; this_->__last = piece;
    }
    
    piece->buffer[piece->size] = ch_; ++(piece->size);
    return;
}


size_t minsonStringBuilderSize(t_minson_string_builder *this_)
{
    size_t total_size = 0;
    
    struct __t_minson_string_piece *cur;
    for(cur = this_->__first; cur != NULL; cur = cur->next)
    {
        total_size += cur->size;
    }
    return total_size;
}


void minsonBuildString(t_minson_string_builder *this_, char *out_string_)
{
    if(out_string_ == NULL) return;
    
    struct __t_minson_string_piece *cur;
    for(cur = this_->__first; cur != NULL; cur = cur->next)
    {
        if(cur->size > 0)
        {
            memcpy((void *)out_string_, (void const *)cur->buffer, cur->size);
            out_string_ += cur->size;
        }
    }
    return;
}
