#ifndef _OFFNET_MINSON_STRING_BUILDER_HPP
#define _OFFNET_MINSON_STRING_BUILDER_HPP

#include "minson_object.hpp"
#include <cstdlib>
#include <cstring>

namespace minson
{


/* Класс строкового буфера для формирования строки. */
class string_builder
{
private:
    t_minson_string_builder *__b;
    
public:
    string_builder() :
        __b(minsonCreateStringBuilder())
    {}
    ~string_builder()
    {   minsonDeleteStringBuilder(__b);   }
    
    string_builder &move(char const *mv, size_t str_size_)
    {
        minsonStringBuilderMove(__b, mv, str_size_); return *this;
    }
    
    string_builder &put(std::string const &str_)
    {
        register size_t str_size = str_.size();
        char *mv = static_cast<char *>(malloc(str_size));
        memcpy(mv, str_.data(), str_size);
        minsonStringBuilderMove(__b, mv, str_size);
        return *this;
    }
    
    string_builder &putChar(char ch_)
    {
        minsonStringBuilderPutChar(__b, ch_); return *this;
    }
    
    string_builder &serialize(object const &object_, bool is_pretty_=false) throw(serialize_error)
    {
        if(is_pretty_)
        {
            if(! minsonPrettySerialize(object_.__obj, __b))
                throw serialize_error{minsonStringBuilderSize(__b)};
        }
        else
        {
            if(! minsonSerialize(object_.__obj, __b))
                throw serialize_error{minsonStringBuilderSize(__b)};
        }
        return *this;
    }
    
    size_t size() const
    {   return minsonStringBuilderSize(__b);   }
    
    std::string build() const
    {
        std::string str(minsonStringBuilderSize(__b), '\0');
        minsonBuildString(__b, const_cast<char *>(str.data()));
        return str;
    }
}; // class string_builder


} // namespace minson

#endif // _OFFNET_MINSON_STRING_BUILDER_HPP
