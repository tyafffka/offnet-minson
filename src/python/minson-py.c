#include "minson_object-py.h"
#include "minson_array-py.h"
#include "minson_iterator-py.h"


static PyObject *__minson_parse(PyObject *m, PyObject *args)
{
    char const *str_;
    if(! PyArg_ParseTuple(args, "s", &str_)) return NULL;
    
    t_minson_object *obj = minsonParse(str_);
    if(obj == NULL)
    {
        char buff[256];
        snprintf(buff, sizeof(buff), "Parsing of string failed at line %d, column %d!",
                 minson_error_line, minson_error_column);
        PyErr_SetString(minson_ParseError, buff);
        return NULL;
    }
    return _minson_instance(obj, true);
}

static PyObject *__minson_parseFile(PyObject *m, PyObject *args)
{
    char const *filename_;
    if(! PyArg_ParseTuple(args, "s", &filename_)) return NULL;
    
    t_minson_object *obj = minsonParseFile(filename_);
    if(obj == NULL)
    {
        char buff[256];
        snprintf(buff, sizeof(buff), "Parsing of file failed at line %d, column %d!",
                 minson_error_line, minson_error_column);
        PyErr_SetString(minson_ParseError, buff);
        return NULL;
    }
    return _minson_instance(obj, true);
}

// ---

static PyObject *__minson_serialize(PyObject *m, PyObject *args)
{
    PyObject *obj_;
    int is_pretty_ = 0;
    if(! PyArg_ParseTuple(args, "O|p", &obj_, &is_pretty_)) return NULL;
    
    if(! PyObject_TypeCheck(obj_, &MinSONObjectClass))
    {
        PyErr_SetString(PyExc_TypeError, "Serialized object must be of 'MinSONObject' type!");
        return NULL;
    }
    
    t_minson_string_builder *builder = minsonCreateStringBuilder();
    uint8_t r = is_pretty_ ? minsonPrettySerialize(selfobj(obj_), builder) :
                             minsonSerialize(selfobj(obj_), builder);
    if(! r)
    {
        minsonDeleteStringBuilder(builder);
        PyErr_SetString(minson_SerializeError, "Serialization failed!");
        return NULL;
    }
    
    size_t out_size = minsonStringBuilderSize(builder);
    char buffer[out_size + 1];
    minsonBuildString(builder, buffer);
    buffer[out_size] = '\0';
    minsonDeleteStringBuilder(builder);
    
    return Py_BuildValue("s", buffer);
}

static PyObject *__minson_serializeFile(PyObject *m, PyObject *args)
{
    PyObject *obj_;
    char const *filename_;
    int is_pretty_ = 0;
    if(! PyArg_ParseTuple(args, "Os|p", &obj_, &filename_, &is_pretty_)) return NULL;
    
    if(! PyObject_TypeCheck(obj_, &MinSONObjectClass))
    {
        PyErr_SetString(PyExc_TypeError, "Serialized object must be of 'MinSONObject' type!");
        return NULL;
    }
    
    uint8_t r = is_pretty_ ? minsonPrettySerializeFile(selfobj(obj_), filename_) :
                             minsonSerializeFile(selfobj(obj_), filename_);
    if(r) Py_RETURN_TRUE;
    else  Py_RETURN_FALSE;
}

// --- *** ---


static PyMethodDef ModuleMethods[] = {
    {"parse", __minson_parse, METH_VARARGS, "parse(str_) -> MinSONObject\nРазбирает MinSON-объект из текстового представления.\n"},
    {"parseFile", __minson_parseFile, METH_VARARGS, "parseFile(filename_) -> MinSONObject\nРазбирает MinSON-объект из файла.\n"},
    {"serialize", __minson_serialize, METH_VARARGS, "serialize(obj_, is_pretty_=False) -> str\nВозвращает текстовое представление объекта.\n"},
    {"serializeFile", __minson_serializeFile, METH_VARARGS, "serializeFile(obj_, filename_, is_pretty_=False) -> bool\nЗаписывает текстовое представление объекта в файл.\n"},
    {NULL, NULL, 0, NULL}
};

#if PYTHON_ABI_VERSION == 3
static PyModuleDef Module = {
    PyModuleDef_HEAD_INIT,
    "minson",
    "Модуль реализации формата данных MinSON.",
    -1,
    ModuleMethods
};
#endif


#if PYTHON_ABI_VERSION == 2
PyMODINIT_FUNC initminson( void )
#elif PYTHON_ABI_VERSION == 3
PyMODINIT_FUNC PyInit_minson( void )
#endif
{
    MinSONArrayClass.tp_dealloc = MinSONObjectClass.tp_dealloc; // necessary?
    MinSONArrayClass.tp_repr = MinSONObjectClass.tp_repr; //-
    MinSONArrayClass.tp_new = MinSONObjectClass.tp_new; //-
    
#if PYTHON_ABI_VERSION == 2
    if(PyType_Ready(&MinSONObjectClass) < 0) return;
    if(PyType_Ready(&MinSONArrayClass) < 0) return;
    if(PyType_Ready(&MinSONIteratorClass) < 0) return;
#elif PYTHON_ABI_VERSION == 3
    if(PyType_Ready(&MinSONObjectClass) < 0) return NULL;
    if(PyType_Ready(&MinSONArrayClass) < 0) return NULL;
    if(PyType_Ready(&MinSONIteratorClass) < 0) return NULL;
#endif
    
#if PYTHON_ABI_VERSION == 2
    PyObject *m = Py_InitModule3("minson", ModuleMethods, "Модуль реализации формата данных MinSON.");
    if(m == NULL) return;
#elif PYTHON_ABI_VERSION == 3
    PyObject *m = PyModule_Create(&Module);
    if(m == NULL) return m;
#endif
    
    // ---
    
    Py_INCREF(&MinSONObjectClass);
    PyModule_AddObject(m, "MinSONObject", m_pcast(PyObject *, &MinSONObjectClass));
    
    Py_INCREF(&MinSONArrayClass);
    PyModule_AddObject(m, "MinSONArray", m_pcast(PyObject *, &MinSONArrayClass));
    
    Py_INCREF(&MinSONIteratorClass);
    
    // ---
    
    minson_InternalError = PyErr_NewExceptionWithDoc(
        "minson.InternalError", "Внутренняя ошибка в работе библиотеки.", NULL, NULL
    );
    Py_INCREF(minson_InternalError);
    PyModule_AddObject(m, "InternalError", minson_InternalError);
    
    minson_ParseError = PyErr_NewExceptionWithDoc(
        "minson.ParseError", "Исключение при ошибке разбора.", NULL, NULL
    );
    Py_INCREF(minson_ParseError);
    PyModule_AddObject(m, "ParseError", minson_ParseError);
    
    minson_SerializeError = PyErr_NewExceptionWithDoc(
        "minson.SerializeError", "Исключение при ошибке сериализации.", NULL, NULL
    );
    Py_INCREF(minson_SerializeError);
    PyModule_AddObject(m, "SerializeError", minson_SerializeError);
    
#if PYTHON_ABI_VERSION == 2
    return;
#elif PYTHON_ABI_VERSION == 3
    return m;
#endif
}
