#include "minson_array-py.h"
#include "minson_iterator-py.h"


static int __MinSONArray_init(PyObject *self, PyObject *args, PyObject *kwargs)
{
    char const *name_;
    if(! PyArg_ParseTuple(args, "z", &name_)) return -1;
    
    oself(self)->__obj = m_pcast(t_minson_object *, minsonCreateArray(name_));
    if(oself(self)->__obj == NULL)
    {
        PyErr_SetString(minson_InternalError, "... in array creation");
        return -1;
    }
    return 0;
}

static PyObject *__MinSONArray_iter(PyObject *self)
{
    PyObject *r = (MinSONIteratorClass.tp_alloc)(&MinSONIteratorClass, 0);
    if(r == NULL) return NULL;
    
    Py_INCREF(self);
    iself(r)->__back_ref = oself(self);
    iself(r)->__iter = minsonFirst(selfarray(self));
    iself(r)->__only_names = false;
    iself(r)->__unnamed = false;
    return r;
}

// ---

static PyObject *__MinSONArray_put(PyObject *self, PyObject *args)
{
    PyObject *obj_;
    if(! PyArg_ParseTuple(args, "O", &obj_)) return NULL;
    
    if(! PyObject_TypeCheck(obj_, &MinSONObjectClass))
    {
        PyErr_SetString(PyExc_TypeError, "Object must be of 'MinSONObject' type!");
        return NULL;
    }
    
    t_minson_object *obj = selfobj(obj_);
    if(! oself(obj_)->__is_own) obj = minsonDuplicate(obj);
    oself(obj_)->__is_own = false;
    
    minsonDelete(minsonArrayRemove(selfarray(self), obj->name));
    minsonArrayAdd(selfarray(self), obj);
    
    Py_INCREF(self);
    return self;
}

static PyObject *__MinSONArray_putValue(PyObject *self, PyObject *args)
{
    char const *name_;
    PyObject *value_;
    if(! PyArg_ParseTuple(args, "zO", &name_, &value_)) return NULL;
    
    t_minson_object *obj = _minson_construct_value(name_, value_);
    if(obj == NULL) return NULL;
    
    minsonDelete(minsonArrayRemove(selfarray(self), obj->name));
    minsonArrayAdd(selfarray(self), obj);
    
    Py_INCREF(self);
    return self;
}

static PyObject *__MinSONArray_putArray(PyObject *self, PyObject *args)
{
    char const *name_;
    if(! PyArg_ParseTuple(args, "z", &name_)) return NULL;
    
    t_minson_object *obj = m_pcast(t_minson_object *, minsonCreateArray(name_));
    if(obj == NULL)
    {
        PyErr_SetString(minson_InternalError, "... in sub-array creation");
        return NULL;
    }
    
    minsonDelete(minsonArrayRemove(selfarray(self), obj->name));
    minsonArrayAdd(selfarray(self), obj);
    
    Py_INCREF(self);
    return self;
}

static PyObject *__MinSONArray_update(PyObject *self, PyObject *args)
{
    PyObject *from_;
    if(! PyArg_ParseTuple(args, "O", &from_)) return NULL;
    
    if(! PyObject_TypeCheck(from_, &MinSONArrayClass))
    {
        PyErr_SetString(PyExc_TypeError, "Update only from object of 'MinSONArrayClass' type!");
        return NULL;
    }
    
    minsonArrayUpdate(selfarray(self), selfarray(from_));
    
    Py_INCREF(self);
    return self;
}

static PyObject *__MinSONArray_pop(PyObject *self, PyObject *args)
{
    char const *name_;
    if(! PyArg_ParseTuple(args, "s", &name_)) return NULL;
    
    t_minson_object *obj = minsonArrayRemove(selfarray(self), name_);
    if(obj == NULL)
    {
        PyErr_SetString(PyExc_KeyError, "MinSON item not found!");
        return NULL;
    }
    return _minson_instance(obj, true);
}

static PyObject *__MinSONArray_namedCount(PyObject *self)
{
    return Py_BuildValue("l", (long)minsonArrayNamedCount(selfarray(self)));
}

static PyObject *__MinSONArray_unnamedCount(PyObject *self)
{
    return Py_BuildValue("l", (long)(
        minsonArrayCount(selfarray(self)) - minsonArrayNamedCount(selfarray(self))
    ));
}

static PyObject *__MinSONArray_iterkeys(PyObject *self)
{
    PyObject *r = (MinSONIteratorClass.tp_alloc)(&MinSONIteratorClass, 0);
    if(r == NULL) return r;
    
    Py_INCREF(self);
    iself(r)->__back_ref = oself(self);
    iself(r)->__iter = minsonFirst(selfarray(self));
    iself(r)->__only_names = true;
    iself(r)->__unnamed = false;
    return r;
}

static PyObject *__MinSONArray_iterunnamed(PyObject *self)
{
    PyObject *r = (MinSONIteratorClass.tp_alloc)(&MinSONIteratorClass, 0);
    if(r == NULL) return r;
    
    Py_INCREF(self);
    iself(r)->__back_ref = oself(self);
    iself(r)->__iter = minsonFirst(selfarray(self));
    iself(r)->__only_names = false;
    iself(r)->__unnamed = true;
    return r;
}

static PyObject *__MinSONArray_iterfrom(PyObject *self, PyObject *args)
{
    char const *name_;
    if(! PyArg_ParseTuple(args, "s", &name_)) return NULL;
    
    PyObject *r = (MinSONIteratorClass.tp_alloc)(&MinSONIteratorClass, 0);
    if(r == NULL) return NULL;
    
    Py_INCREF(self);
    iself(r)->__back_ref = oself(self);
    iself(r)->__iter = minsonArrayGetIter(selfarray(self), name_);
    iself(r)->__only_names = false;
    iself(r)->__unnamed = false;
    return r;
}

// ---

static Py_ssize_t __MinSONArray_len(PyObject *self)
{
    return (Py_ssize_t)minsonArrayCount(selfarray(self));
}

static PyObject *__MinSONArray_getitem(PyObject *self, PyObject *name_)
{
#if PYTHON_ABI_VERSION == 2
    char const *name = PyString_AsString(name_);
#elif PYTHON_ABI_VERSION == 3
    char const *name = _PyUnicode_AsString(name_);
#endif
    if(name == NULL) return NULL;
    
    t_minson_object *item = minsonArrayGet(selfarray(self), name);
    if(item == NULL)
    {
        PyErr_SetString(PyExc_KeyError, "MinSON item not found!");
        return NULL;
    }
    return _minson_extract_value(item);
}

static int __MinSONArray_setitem(PyObject *self, PyObject *name_, PyObject *value_)
{
#if PYTHON_ABI_VERSION == 2
    char const *name = PyString_AsString(name_);
#elif PYTHON_ABI_VERSION == 3
    char const *name = _PyUnicode_AsString(name_);
#endif
    if(name == NULL) return -1;
    
    if(value_ != NULL)
    {
        t_minson_object *obj;
        if(PyObject_TypeCheck(value_, &MinSONObjectClass))
        {
            obj = selfobj(value_);
            if(! oself(value_)->__is_own) obj = minsonDuplicate(obj);
            oself(value_)->__is_own = false;
        }
        else
        {
            obj = _minson_construct_value(name, value_);
            if(obj == NULL) return -1;
        }
        
        minsonDelete(minsonArrayRemove(selfarray(self), obj->name));
        minsonArrayAdd(selfarray(self), obj);
    }
    else // Delete item.
    {   minsonDelete(minsonArrayRemove(selfarray(self), name));   }
    return 0;
}

static int __MinSONArray_contains(PyObject *self, PyObject *name_)
{
#if PYTHON_ABI_VERSION == 2
    char const *name = PyString_AsString(name_);
#elif PYTHON_ABI_VERSION == 3
    char const *name = _PyUnicode_AsString(name_);
#endif
    if(name == NULL) return -1;
    
    return (minsonArrayGetIter(selfarray(self), name) != NULL)? 1 : 0;
}

// --- *** ---


static PyMemberDef MinSONArrayMembers[] = {
    {NULL}
};

static PyMethodDef MinSONArrayMethods[] = {
    {"put", __MinSONArray_put, METH_VARARGS, "put(self, obj_) -> self\nПереместить объект в массив. Возвращает `self`.\n"},
    {"putValue", __MinSONArray_putValue, METH_VARARGS, "putValue(self, name_, value_) -> self\nСоздать вложенный объект. Возвращает `self`.\n"},
    {"putArray", __MinSONArray_putArray, METH_VARARGS, "putArray(self, name_) -> self\nСоздать пустой вложенный массив. Возвращает `self`.\n"},
    {"update", __MinSONArray_update, METH_VARARGS, "update(self, from_) -> self\nДополняет массив из другого массива. Возвращает `self`.\n"},
    {"pop", __MinSONArray_pop, METH_VARARGS, "pop(self, name_) -> MinSONObject\nИзвлекает объект из массива по имени.\n"},
    {"namedCount", (PyCFunction)__MinSONArray_namedCount, METH_NOARGS, "namedCount(self) -> int\nВозвращает число именованных элементов.\n"},
    {"unnamedCount", (PyCFunction)__MinSONArray_unnamedCount, METH_NOARGS, "unnamedCount(self) -> int\nВозвращает число безымянных элементов.\n"},
    {"iterkeys", (PyCFunction)__MinSONArray_iterkeys, METH_NOARGS, "iterkeys(self) -> iter[str]\nИтератор по именам элементов.\n"},
    {"iterunnamed", (PyCFunction)__MinSONArray_iterunnamed, METH_NOARGS, "iterunnamed(self) -> iter[str | int | float | bool | None | MinSONArray]\nИтератор по значениям безымянных элементов.\n"},
    {"iterfrom", (PyCFunction)__MinSONArray_iterfrom, METH_VARARGS, "iterfrom(self, name_) -> iter[MinSONObject]\nИтератор по элементам, начиная от элемента с указанным именем.\n"},
    {NULL, NULL, 0, NULL}
};

static PySequenceMethods MinSONArraySequenceMethods = {
    __MinSONArray_len, /* sq_length */
    0, /* sq_concat */
    0, /* sq_repeat */
    0, /* sq_item */
    0, /* sq_slice */
    0, /* sq_ass_item */
    0, /* sq_ass_slice */
    __MinSONArray_contains, /* sq_contains */
};

static PyMappingMethods MinSONArrayMappingMethods = {
    __MinSONArray_len, /* mp_length */
    __MinSONArray_getitem, /* mp_subscript */
    __MinSONArray_setitem, /* mp_ass_subscript */
};

PyTypeObject MinSONArrayClass = {
    PyVarObject_HEAD_INIT(NULL, 0)
    "minson.MinSONArray",     /* tp_name */
    sizeof(struct __t_MinSONObject), /* tp_basicsize */
    0,                         /* tp_itemsize */
    0, /* tp_dealloc : inherit */
    0,                         /* tp_print */
    0,                         /* tp_getattr */
    0,                         /* tp_setattr */
    0,                         /* tp_compare */
    0, /* tp_repr : inherit */
    0,                         /* tp_as_number */
    &MinSONArraySequenceMethods, /* tp_as_sequence */
    &MinSONArrayMappingMethods, /* tp_as_mapping */
    0,                         /* tp_hash */
    0,                         /* tp_call */
    0,                         /* tp_str */
    0,                         /* tp_getattro */
    0,                         /* tp_setattro */
    0,                         /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* tp_flags */
    "MinSONArray(name_) -> MinSONArray\nКласс MinSON-массива.\n", /* tp_doc */
    0,                         /* tp_traverse */
    0,                         /* tp_clear */
    0,                         /* tp_richcompare */
    0,                         /* tp_weaklistoffset */
    __MinSONArray_iter, /* tp_iter */
    0,                         /* tp_iternext */
    MinSONArrayMethods, /* tp_methods */
    MinSONArrayMembers, /* tp_members */
    0,                         /* tp_getset */
    &MinSONObjectClass, /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    __MinSONArray_init, /* tp_init */
    0,                   /* tp_alloc */
    0, /* tp_new : inherit */
};
