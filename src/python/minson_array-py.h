#ifndef __MINSON_ARRAY_PY_H
#define __MINSON_ARRAY_PY_H

#include "minson_object-py.h"


#define selfarray(self) m_pcast(t_minson_array *, selfobj(self))


extern PyTypeObject MinSONArrayClass;


#endif // __MINSON_ARRAY_PY_H
