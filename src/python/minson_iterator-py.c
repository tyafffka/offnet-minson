#include "minson_iterator-py.h"


static PyObject *__MinSONIterator_new(PyTypeObject *cls, PyObject *args, PyObject *kwargs)
{
    PyObject *self = (cls->tp_alloc)(cls, 0);
    if(self == NULL) return self;
    
    iself(self)->__back_ref = NULL;
    iself(self)->__iter = NULL;
    iself(self)->__only_names = false;
    iself(self)->__unnamed = false;
    return self;
}

static int __MinSONIterator_init(PyObject *self, PyObject *args, PyObject *kwargs)
{   return 0;   }

static void __MinSONIterator_delete(PyObject *self)
{
    struct __t_MinSONObject *back_ref__ = iself(self)->__back_ref;
    iself(self)->__back_ref = NULL;
    Py_XDECREF(back_ref__);
    
    (Py_TYPE(self)->tp_free)(self);
    return;
}

// ---

static PyObject *__MinSONIterator_iter(PyObject *self)
{
    Py_INCREF(self); return self;
}

static PyObject *__MinSONIterator_iternext(PyObject *self)
{
    if(selfiter(self) == NULL)
    {
        PyErr_SetNone(PyExc_StopIteration); return NULL;
    }
    
    PyObject *r = NULL;
    
    if(iself(self)->__only_names)
    {
        for(; minsonNotEnd(selfiter(self)); minsonNext(selfiter(self)))
            if(minsonIterName(selfiter(self)) != NULL) break;
        
        if(selfiter(self) == NULL)
        {
            PyErr_SetNone(PyExc_StopIteration); return NULL;
        }
        
        r = Py_BuildValue("s", minsonIterName(selfiter(self)));
    }
    else if(iself(self)->__unnamed)
    {
        for(; minsonNotEnd(selfiter(self)); minsonNext(selfiter(self)))
            if(minsonIterName(selfiter(self)) == NULL) break;
        
        if(selfiter(self) == NULL)
        {
            PyErr_SetNone(PyExc_StopIteration); return NULL;
        }
        
        r = _minson_extract_value(selfiterobj(self));
    }
    else
    {   r = _minson_instance(selfiterobj(self), false);   }
    
    minsonNext(selfiter(self));
    return r;
}

// --- *** ---


static PyMemberDef MinSONIteratorMembers[] = {
    {NULL}
};

static PyMethodDef MinSONIteratorMethods[] = {
    {NULL, NULL, 0, NULL}
};

PyTypeObject MinSONIteratorClass = {
    PyVarObject_HEAD_INIT(NULL, 0)
    "minson.MinSONIterator",     /* tp_name */
    sizeof(struct __t_MinSONIterator), /* tp_basicsize */
    0,                         /* tp_itemsize */
    __MinSONIterator_delete, /* tp_dealloc */
    0,                         /* tp_print */
    0,                         /* tp_getattr */
    0,                         /* tp_setattr */
    0,                         /* tp_compare */
    0, /* tp_repr */
    0,                         /* tp_as_number */
    0,                         /* tp_as_sequence */
    0,                         /* tp_as_mapping */
    0,                         /* tp_hash */
    0,                         /* tp_call */
    0,                         /* tp_str */
    0,                         /* tp_getattro */
    0,                         /* tp_setattro */
    0,                         /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT, /* tp_flags */
    "Класс итератора по MinSON-массиву.", /* tp_doc */
    0,                         /* tp_traverse */
    0,                         /* tp_clear */
    0,                         /* tp_richcompare */
    0,                         /* tp_weaklistoffset */
    __MinSONIterator_iter, /* tp_iter */
    __MinSONIterator_iternext, /* tp_iternext */
    MinSONIteratorMethods, /* tp_methods */
    MinSONIteratorMembers, /* tp_members */
    0,                         /* tp_getset */
    0, /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    __MinSONIterator_init, /* tp_init */
    0,                   /* tp_alloc */
    __MinSONIterator_new, /* tp_new */
};
