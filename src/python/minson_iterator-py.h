#ifndef __MINSON_ITERATOR_PY_H
#define __MINSON_ITERATOR_PY_H

#include "minson_object-py.h"


struct __t_MinSONIterator
{
    PyObject_HEAD
    struct __t_MinSONObject *__back_ref;
    t_minson_iter __iter;
    bool __only_names, __unnamed;
};

#define iself(self) m_pcast(struct __t_MinSONIterator *, self)
#define selfiter(self) (iself(self)->__iter)
#define selfiterobj(self) (iself(self)->__iter->_object)


extern PyTypeObject MinSONIteratorClass;


#endif // __MINSON_ITERATOR_PY_H
