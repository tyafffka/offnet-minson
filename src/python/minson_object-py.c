#include "minson_object-py.h"
#include "minson_array-py.h"


PyObject *_minson_instance(t_minson_object *obj_, bool is_own_)
{
    if(obj_ == NULL)
    {
        PyErr_SetString(minson_InternalError, "... in instantiation");
        return NULL;
    }
    
    PyObject *self;
    if(obj_->type == C_MINSON_ARRAY)
        self = (MinSONArrayClass.tp_alloc)(&MinSONArrayClass, 0);
    else
        self = (MinSONObjectClass.tp_alloc)(&MinSONObjectClass, 0);
    
    if(self == NULL)
    {
        if(is_own_) minsonDelete(obj_);
        return NULL;
    }
    
    oself(self)->__obj = obj_;
    oself(self)->__is_own = is_own_;
    return self;
}

PyObject *_minson_extract_value(t_minson_object *obj_)
{
    switch(obj_->type)
    {
    case C_MINSON_ARRAY:
        return _minson_instance(obj_, false);
    case C_MINSON_STRING:
        return Py_BuildValue("s", m_pcast(t_minson_string *, obj_)->value);
    case C_MINSON_NUMBER:
        return Py_BuildValue("l", (long)(m_pcast(t_minson_number *, obj_)->value));
    case C_MINSON_FLOAT_NUMBER:
        return Py_BuildValue("d", m_pcast(t_minson_float_number *, obj_)->value);
    case C_MINSON_BOOLEAN:
        if(m_pcast(t_minson_boolean *, obj_)->value)
            Py_RETURN_TRUE;
        else
            Py_RETURN_FALSE;
    case C_MINSON_NULL:
        Py_RETURN_NONE;
    }
}

t_minson_object *_minson_construct_value(char const *name_, PyObject *value_)
{
    if(value_ == Py_None)
    {   return minsonCreateNull(name_);   }
    else if(value_ == Py_True)
    {   return minsonCreateBoolean(name_, 1);   }
    else if(value_ == Py_False)
    {   return minsonCreateBoolean(name_, 0);   }
#if PYTHON_ABI_VERSION == 2
    else if(PyObject_TypeCheck(value_, &PyInt_Type))
    {   return minsonCreateNumber(name_, (int64_t)PyInt_AsLong(value_));   }
#elif PYTHON_ABI_VERSION == 3
    else if(PyObject_TypeCheck(value_, &PyLong_Type))
    {   return minsonCreateNumber(name_, (int64_t)PyLong_AsLong(value_));   }
#endif
    else if(PyObject_TypeCheck(value_,&PyFloat_Type))
    {   return minsonCreateFloatNumber(name_, PyFloat_AsDouble(value_));   }
#if PYTHON_ABI_VERSION == 2
    else if(PyObject_TypeCheck(value_, &PyString_Type))
    {   return minsonCreateString(name_, PyString_AsString(value_));   }
#elif PYTHON_ABI_VERSION == 3
    else if(PyObject_TypeCheck(value_, &PyUnicode_Type))
    {   return minsonCreateString(name_, _PyUnicode_AsString(value_));   }
#endif
    else
    {
        PyErr_SetString(PyExc_ValueError, "MinSONObject value must be str, int, float, bool or None!");
        return NULL;
    }
}

// ---

static PyObject *__MinSONObject_new(PyTypeObject *cls, PyObject *args, PyObject *kwargs)
{
    PyObject *self = (cls->tp_alloc)(cls, 0);
    if(self == NULL) return self;
    
    oself(self)->__obj = NULL;
    oself(self)->__is_own = true;
    return self;
}

static int __MinSONObject_init(PyObject *self, PyObject *args, PyObject *kwargs)
{
    char const *name_;
    PyObject *value_;
    if(! PyArg_ParseTuple(args, "zO", &name_, &value_)) return -1;
    
    oself(self)->__obj = _minson_construct_value(name_, value_);
    return (oself(self)->__obj != NULL)? 0 : -1;
}

static void __MinSONObject_delete(PyObject *self)
{
    if(oself(self)->__is_own) minsonDelete(selfobj(self));
    
    (Py_TYPE(self)->tp_free)(self);
    return;
}

// ---

static PyObject *__MinSONObject_hasName(PyObject *self)
{
    if(selfobj(self)->name != NULL)
        Py_RETURN_TRUE;
    else
        Py_RETURN_FALSE;
}

static PyObject *__MinSONObject_name(PyObject *self)
{
    return Py_BuildValue("z", selfobj(self)->name);
}

static PyObject *__MinSONObject_value(PyObject *self)
{
    if(selfobj(self)->type == C_MINSON_ARRAY)
    {
        Py_INCREF(self); return self;
    }
    return _minson_extract_value(selfobj(self));
}

static PyObject *__MinSONObject_copy(PyObject *self)
{   return _minson_instance(minsonDuplicate(selfobj(self)), true);   }

// ---

static PyObject *__MinSONObject_repr(PyObject *self)
{
    char buffer[200];
    
    switch(selfobj(self)->type)
    {
    case C_MINSON_ARRAY:
        snprintf(buffer, 200, "<MinSONArray %s[%d item(s)]>",
                 (selfobj(self)->name != NULL)? selfobj(self)->name : "",
                 minsonArrayCount(m_pcast(t_minson_array *, selfobj(self))));
        break;
        
    case C_MINSON_STRING:
    {
        char const *s = m_pcast(t_minson_string *, selfobj(self))->value;
        char const *s2 = s;
        int len__ = 0;
        while(*s2++) if(++len__ > 10) break;
        
        if(len__ > 10)
            snprintf(buffer, 200, "<MinSONObject %s[string] '%.*s'...>",
                     (selfobj(self)->name != NULL)? selfobj(self)->name : "",
                     7, s);
        else
            snprintf(buffer, 200, "<MinSONObject %s[string] '%s'>",
                     (selfobj(self)->name != NULL)? selfobj(self)->name : "",
                     s);
        break;
    }
    case C_MINSON_NUMBER:
        snprintf(buffer, 200, "<MinSONObject %s[number] %ld>",
                 (selfobj(self)->name != NULL)? selfobj(self)->name : "",
                 m_pcast(t_minson_number *, selfobj(self))->value);
        break;
        
    case C_MINSON_FLOAT_NUMBER:
        snprintf(buffer, 200, "<MinSONObject %s[float] %#.12g>",
                 (selfobj(self)->name != NULL)? selfobj(self)->name : "",
                 m_pcast(t_minson_float_number *, selfobj(self))->value);
        break;
        
    case C_MINSON_BOOLEAN:
        snprintf(buffer, 200, "<MinSONObject %s[boolean] %s>",
                 (selfobj(self)->name != NULL)? selfobj(self)->name : "",
                 (m_pcast(t_minson_boolean *, selfobj(self))->value)? "yes" : "no");
        break;
        
    case C_MINSON_NULL:
        snprintf(buffer, 200, "<MinSONObject %s[null]>",
                 (selfobj(self)->name != NULL)? selfobj(self)->name : "");
        break;
    }
    return Py_BuildValue("s", buffer);
}

// --- *** ---


PyObject *minson_InternalError;
PyObject *minson_ParseError;
PyObject *minson_SerializeError;


static PyMemberDef MinSONObjectMembers[] = {
    {NULL}
};

static PyMethodDef MinSONObjectMethods[] = {
    {"hasName", (PyCFunction)__MinSONObject_hasName, METH_NOARGS, "hasName(self) -> bool\nПроверяет, есть ли у объекта имя.\n"},
    {"name", (PyCFunction)__MinSONObject_name, METH_NOARGS, "name(self) -> str | None\nВозвращает имя объекта, если оно есть.\n"},
    {"value", (PyCFunction)__MinSONObject_value, METH_NOARGS, "value(self) -> str | int | float | bool | None\nВозвращает значение объекта соответствующего типа.\n"},
    {"copy", (PyCFunction)__MinSONObject_copy, METH_NOARGS, "copy(self) -> MinSONObject\nВозвращает копию MinSON-объекта со всеми данными.\n"},
    {NULL, NULL, 0, NULL}
};

PyTypeObject MinSONObjectClass = {
    PyVarObject_HEAD_INIT(NULL, 0)
    "minson.MinSONObject",     /* tp_name */
    sizeof(struct __t_MinSONObject), /* tp_basicsize */
    0,                         /* tp_itemsize */
    __MinSONObject_delete, /* tp_dealloc */
    0,                         /* tp_print */
    0,                         /* tp_getattr */
    0,                         /* tp_setattr */
    0,                         /* tp_compare */
    __MinSONObject_repr, /* tp_repr */
    0,                         /* tp_as_number */
    0,                         /* tp_as_sequence */
    0,                         /* tp_as_mapping */
    0,                         /* tp_hash */
    0,                         /* tp_call */
    0,                         /* tp_str */
    0,                         /* tp_getattro */
    0,                         /* tp_setattro */
    0,                         /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* tp_flags */
    "MinSONObject(name_, value_) -> MinSONObject\nКласс MinSON-объекта.\n", /* tp_doc */
    0,                         /* tp_traverse */
    0,                         /* tp_clear */
    0,                         /* tp_richcompare */
    0,                         /* tp_weaklistoffset */
    0,                         /* tp_iter */
    0,                         /* tp_iternext */
    MinSONObjectMethods, /* tp_methods */
    MinSONObjectMembers, /* tp_members */
    0,                         /* tp_getset */
    0,                         /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    __MinSONObject_init, /* tp_init */
    0,                   /* tp_alloc */
    __MinSONObject_new, /* tp_new */
};
