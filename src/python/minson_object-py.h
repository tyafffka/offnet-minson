#ifndef __MINSON_OBJECT_PY_H
#define __MINSON_OBJECT_PY_H

#include "m.h"
#include <minson.h>
#include <Python.h>
#include <structmember.h>
#include <stdbool.h>


struct __t_MinSONObject
{
    PyObject_HEAD
    t_minson_object *__obj;
    bool __is_own;
};

#define oself(self) m_pcast(struct __t_MinSONObject *, self)
#define selfobj(self) (oself(self)->__obj)


PyObject *_minson_instance(t_minson_object *obj_, bool is_own_);
PyObject *_minson_extract_value(t_minson_object *obj_);
t_minson_object *_minson_construct_value(char const *name_, PyObject *value_);


extern PyObject *minson_InternalError;
extern PyObject *minson_ParseError;
extern PyObject *minson_SerializeError;

extern PyTypeObject MinSONObjectClass;


#endif // __MINSON_OBJECT_PY_H
