# -*- coding: utf-8 -*-


__all__ = ('MinSONError', 'ParseError', 'MinSONObject', 'MinSONArray',
           'parse', 'parseFile', 'serialize', 'serializeFile')


class MinSONError(ValueError): pass
# class InternalError(MinSONError): pass
class ParseError(MinSONError): pass
# class SerializeError(MinSONError): pass


class MinSONObject:
    __slots__ = ('_name', '_value')
    def __init__(self, name_, value_):
        self._name = name_
        self._value = value_
    
    def __repr__(self):
        name = self._name or ""
        if isinstance(self._value, str):
            if len(self._value) > 10:
                return "<MinSONObject {}[string] '{}'...>".format(name, self._value[:7])
            else:
                return "<MinSONObject {}[string] '{}'>".format(name, self._value)
        elif isinstance(self._value, bool):
            return "<MinSONObject {}[boolean] {}>".format(name, "yes" if self._value else "no")
        elif isinstance(self._value, int):
            return "<MinSONObject {}[number] {}>".format(name, self._value)
        elif isinstance(self._value, float):
            return "<MinSONObject {}[float] {}>".format(name, self._value)
        elif self._value is None:
            return "<MinSONObject {}[null]>".format(name)
        return "<MinSONObject {}[###corrupted!!!]>".format(name)
    
    def hasName(self): return bool(self._name)
    
    def name(self): return self._name
    
    def value(self): return self._value
    
    def __call__(self): return self._name, self._value
    
    def copy(self): return MinSONObject(self._name, self._value)


class MinSONArray(MinSONObject):
    class _Node:
        __slots__ = ('next', 'prev', 'obj')
        def __init__(self, obj_, next_=None, prev_=None):
            self.next = next_
            self.prev = prev_
            self.obj = obj_
    
    __slots__ = ('_first', '_last', '_count')
    def __init__(self, name_=None):
        super(MinSONArray, self).__init__(name_, {})
        self._first = None
        self._last = None
        self._count = 0
    
    def __del__(self):
        last = self._last
        self._last = None
        while last is not None:
            curr = last.prev
            last.prev = None
            last = curr
    
    def __repr__(self):
        return "<MinSONArray {}[{} item(s)]>".format(self._name or "", self._count)
    
    def copy(self):
        r = MinSONArray(self._name)
        for obj in self: r.put(obj)
        return r
    
    def put(self, obj_):
        node = self._Node(obj_, None, self._last)
        if self._last is not None: self._last.next = node
        self._last = node
        if self._first is None: self._first = node
        
        self._count += 1
        if obj_.hasName(): self._value[obj_.name()] = node
        return self
    
    def putValue(self, name_, value_):
        return self.put(MinSONObject(name_, value_))
    
    def putArray(self, name_):
        return self.put(MinSONArray(name_))
    
    def update(self, from_):
        for obj in from_:
            if not obj.hasName():
                self.put(obj)
                continue
            
            node = self._value.get(obj.name(), None)
            if node is None:
                self.put(obj)
                continue
            
            if isinstance(node.obj, MinSONArray) and isinstance(obj, MinSONArray):
                node.obj.update(obj)
            else:
                node.obj = obj
        
        return self
    
    def __setitem__(self, name_, value_):
        self.put(value_ if isinstance(value_, MinSONObject) else MinSONObject(name_, value_))
    
    def __contains__(self, name_): return name_ in self._value
    
    def __getitem__(self, name_): return self._value[name_].obj
    
    def __delitem__(self, name_): self.pop(name_)
    
    def pop(self, name_):
        node = self._value.pop(name_)
        self._count -= 1
        
        if node.next is not None: node.next.prev = node.prev
        if node.prev is not None: node.prev.next = node.next
        if self._first is node: self._first = node.next
        if self._last is node: self._last = node.prev
        
        return node.obj
    
    def __len__(self): return self._count
    
    def namedCount(self): return len(self._value)
    
    def unnamedCount(self): return self._count - len(self._value)
    
    def __iter__(self):
        curr = self._first
        while curr is not None:
            yield curr.obj
            curr = curr.next
    
    def __reversed__(self):
        curr = self._last
        while curr is not None:
            yield curr.obj
            curr = curr.prev
    
    def iterkeys(self):
        for k in self._value: yield k
    
    def iterunnamed(self):
        for obj in self: # type: MinSONObject
            if not obj.hasName():
                yield obj if isinstance(obj, MinSONArray) else obj._value
    
    def iterfrom(self, name_):
        curr = self._value[name_]
        while curr is not None:
            yield curr.obj
            curr = curr.next


def parse(str_):
    parse_iter = _ParseIter(str_).next()
    if parse_iter.token_type is None: raise ParseError("Empty MinSON")
    
    obj = _parse_step(parse_iter)
    if obj is None or parse_iter.next().token_type is not None:
        raise ParseError("Wrong syntax at line {} column {}".format(parse_iter.at_line, parse_iter.at_column))
    return obj


def parseFile(filename_):
    with open(filename_, "rt") as f:
        parse_iter = _ParseIter(f).next()
        if parse_iter.token_type is None: raise ParseError("Empty MinSON")
        
        obj = _parse_step(parse_iter)
        if obj is None:
            raise ParseError("Wrong syntax at line {} column {}".format(parse_iter.at_line, parse_iter.at_column))


def serialize(obj_, is_pretty_=False):
    return ''.join(_serialize_step(
        obj_, [], (lambda pieces_, i: pieces_.append(i)),
        (0 if is_pretty_ else -1)
    ))


def serializeFile(obj_, filename_, is_pretty_=False):
    with open(filename_, "wt") as f:
        _serialize_step(
            obj_, f, (lambda f_, i: f_.write(i)),
            (0 if is_pretty_ else -1)
        )
    return True

# --- *** ---


_TOKEN_IDENTIFIER, _TOKEN_STRING, _TOKEN_BASIC, \
_TOKEN_ARRAY_BEGIN, _TOKEN_ARRAY_END = range(5)

_IS_IDENTIFIER_CHAR = (
#   0                             15
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
#  16                             31
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
#  sp ! " # $ % & ' ( ) * + , - . /
    0,1,0,0,1,0,0,0,0,0,1,1,0,1,1,1,
#   0 1 2 3 4 5 6 7 8 9 : ; < = > ?
    1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,1,
#   @ A B C D E F G H I J K L M N O
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
#   P Q R S T U V W X Y Z [ \ ] ^ _
    1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,1,
#   ` a b c d e f g h i j k l m n o
    0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
#   p q r s t u v w x y z { | } ~ 127
    1,1,1,1,1,1,1,1,1,1,1,0,0,0,1,0
)

_WHITESPACES = (' ', '\t', '\v', '\n', '\r', '\x1b')

_ORD_0, _ORD_9, _ORD_a, _ORD_f, _ORD_A, _ORD_F = \
    ord('0'), ord('9'), ord('a'), ord('f'), ord('A'), ord('F')

_STRING_ESCAPES = {
    ord('\t'): "\\t",
    ord('\v'): "\\v",
    ord('\n'): "\\n",
    ord('\r'): "\\r",
    ord('\x1b'): "\\e",
    ord('\"'): "\\\"",
    ord('\\'): "\\\\",
}


class _ParseIter:
    def __init__(self, str_or_stream_):
        if isinstance(str_or_stream_, str):
            self.__str = str_or_stream_
            self.__get_i = 0
            self._get = self.__get_str
        elif hasattr(str_or_stream_, 'read'):
            self.__stream = str_or_stream_
            self._get = self.__get_stream
        else:
            raise TypeError("That must be a string or stream to parse")
        
        self.buffer_ch = None
        self.token_type = None
        self.token_value = ""
        self.at_line = 1
        self.at_column = 0
    
    def __get_str(self):
        if self.__get_i >= len(self.__str): return None
        ch = self.__str[self.__get_i]
        self.__get_i += 1
        return ch
    
    def __get_stream(self):
        ch = self.__stream.read(1)
        return ch if ch else None
    
    def next(self):
        self.token_type = None
        
        is_comment = False
        is_escape = False
        escape_hex = None
        
        while True:
            if self.buffer_ch:
                ch, self.buffer_ch = self.buffer_ch, None
            else:
                ch = self._get()
                if ch == '\n':
                    self.at_line, self.at_column = (self.at_line + 1), 0
                else:
                    self.at_column += 1
            
            if is_comment:
                if not ch:
                    if self.token_type is None: return self
                    else: raise ParseError("Wrong value at line {} column {}".format(self.at_line, self.at_column))
                
                if ch == '\n' or ch == '\r': is_comment = False
                continue
            
            ord_ch = ch and ord(ch) or 0
            is_whitespace = ch in _WHITESPACES
            is_identifier = ord_ch < 128 and _IS_IDENTIFIER_CHAR[ord_ch] != 0
            
            if self.token_type is None:
                if is_whitespace: pass
                elif ch == '#': is_comment = True
                elif is_identifier:
                    self.token_type = _TOKEN_IDENTIFIER
                    self.token_value = ch
                elif ch == '"':
                    self.token_type = _TOKEN_STRING
                    self.token_value = ""
                elif ch == '(':
                    self.token_type = _TOKEN_BASIC
                    self.token_value = ""
                elif ch == '{':
                    self.token_type = _TOKEN_ARRAY_BEGIN
                    self.token_value = ""
                    return self
                elif ch == '}':
                    self.token_type = _TOKEN_ARRAY_END
                    self.token_value = ""
                    return self
                elif not ch:
                    return self
                else:
                    raise ParseError("Unknow token at line {} column {}".format(self.at_line, self.at_column))
            
            elif self.token_type is _TOKEN_IDENTIFIER:
                if is_identifier:
                    self.token_value += ch
                else:
                    self.buffer_ch = ch
                    return self
            
            elif self.token_type is _TOKEN_STRING:
                if escape_hex is not None:
                    if _ORD_0 <= ord_ch <= _ORD_9: a = ord_ch - _ORD_0
                    elif _ORD_a <= ord_ch <= _ORD_f: a = ch - _ORD_a + 10
                    elif _ORD_A <= ord_ch <= _ORD_F: a = ch - _ORD_A + 10
                    else:
                        self.token_value += 'x' + ch
                        escape_hex = None
                        continue
                    
                    if escape_hex == -2: escape_hex = a << 4
                    else:
                        self.token_value += chr(escape_hex | a)
                        escape_hex = None
                elif is_escape:
                    if ch == 't': self.token_value += '\t'
                    elif ch == 'v': self.token_value += '\v'
                    elif ch == 'n': self.token_value += '\n'
                    elif ch == 'r': self.token_value += '\r'
                    elif ch == 'e': self.token_value += '\x1b'
                    elif ch == 'x': escape_hex = -2
                    else: self.token_value += ch
                    is_escape = False
                elif ch == '\\':
                    is_escape = True
                elif ch == '"':
                    return self
                elif not ch:
                    raise ParseError("Unexpected string end at line {} column {}".format(self.at_line, self.at_column))
                else:
                    self.token_value += ch
            
            elif self.token_type is _TOKEN_BASIC:
                if ch == ')':
                    return self
                elif ch == '#':
                    is_comment = True
                elif not is_whitespace:
                    self.token_value += ch


def _parse_basic(value_):
    if value_ == 'y' or value_ == 'Y': return True
    elif value_ == 'n' or value_ == 'N': return False
    elif value_ == '*': return None
    
    i, sign = 0, 1
    if value_[0] == '+': i = 1
    elif value_[0] == '-': i, sign = 1, -1
    
    if value_[i] == 'b': return sign * int(value_[i+1:], 2)
    if value_[i] == 'o': return sign * int(value_[i+1:], 8)
    if value_[i] == 'x': return sign * int(value_[i+1:], 16)
    if '.' in value_: return sign * float(value_[i:])
    return sign * int(value_[i:], 10)


def _parse_step(parse_iter_):
    if parse_iter_.token_type is _TOKEN_ARRAY_END: return None
    
    name = None
    if parse_iter_.token_type is _TOKEN_IDENTIFIER:
        name = parse_iter_.token_value
        parse_iter_.next()
    
    if parse_iter_.token_type is _TOKEN_STRING:
        return MinSONObject(name, parse_iter_.token_value)
    
    elif parse_iter_.token_type is _TOKEN_BASIC:
        return MinSONObject(name, _parse_basic(parse_iter_.token_value))
    
    elif parse_iter_.token_type is _TOKEN_ARRAY_BEGIN:
        array = MinSONArray(name)
        
        while True:
            subobject = _parse_step(parse_iter_.next())
            if subobject is None: break
            
            array.put(subobject)
        
        # if parse_iter_.token_type is not _TOKEN_ARRAY_END:
        #     raise ParseError("Wrong syntax in array at line {} column {}".format(parse_iter_.at_line, parse_iter_.at_column))
        return array
    
    else:
        raise ParseError("Wrong syntax at line {} column {}".format(parse_iter_.at_line, parse_iter_.at_column))


def _serialize_step(obj_, write_to_, write_cb_, pretty_level_):
    if pretty_level_ > 0: write_cb_(write_to_, "  " * pretty_level_)
    
    name, value = obj_()
    if name:
        write_cb_(write_to_, name)
        if pretty_level_ >= 0: write_cb_(write_to_, " ")
    
    if isinstance(obj_, MinSONArray):
        write_cb_(write_to_, "{")
        if pretty_level_ >= 0:
            write_cb_(write_to_, "\n")
            pretty_level_ += 1
        
        for subobj in obj_:
            _serialize_step(subobj, write_to_, write_cb_, pretty_level_)
        
        if pretty_level_ > 0:
            pretty_level_ -= 1
            if pretty_level_ > 0: write_cb_(write_to_, "  " * pretty_level_)
        write_cb_(write_to_, "}")
    
    elif isinstance(value, str):
        write_cb_(write_to_, "\"")
        write_cb_(write_to_, value.translate(_STRING_ESCAPES))
        write_cb_(write_to_, "\"")
    
    elif isinstance(value, bool):
        write_cb_(write_to_, "(%s)" % ('y' if value else 'n'))
    
    elif isinstance(value, int):
        if pretty_level_ >= 0:
            write_cb_(write_to_, "(%ld)" % value)
        else:
            write_cb_(write_to_, "(%sx%lX)" % (
                "-" if value < 0 else "", abs(value)
            ))
    
    elif isinstance(value, float):
        write_cb_(write_to_, "(%#.12g)" % value)
    
    elif value is None:
        write_cb_(write_to_, "(*)")
    
    if pretty_level_ >= 0: write_cb_(write_to_, "\n")
    return write_to_
