#include "minson.hpp"
#include "test.h"


int main(int argc, char *const *argv)
{
    _TEST_(argc == 3);
    try
    {
        minson::object object__ = minson::parseFile(argv[1]);
        _TEST_(minson::serializeFile(object__, argv[2]));
    }
    catch(minson::parse_error &)
    {
        _FAIL_("Can't parsing!");
    }
    _ENDTEST_;
}
