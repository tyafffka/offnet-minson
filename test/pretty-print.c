#include "m.h"
#include "minson.h"
#include "test.h"


int main(int argc, char *const *argv)
{
    _TEST_(argc == 2);
    
    t_minson_object *object = minsonParseFile(argv[1]);
    _TEST_(object != NULL);
    
    _TEST_(minsonPrettySerializeStream(object, stdout));
    minsonDelete(object);
    
    _ENDTEST_;
}
