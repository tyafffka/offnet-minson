#include "m.h"
#include "_parse.h"
#include "_parse_basic.h"
#include "test.h"


static char *__value_buffer = NULL;

static char *__new_value(t_minson_string_builder *b_)
{
    if(__value_buffer != NULL) m_delete(__value_buffer);
    
    size_t value_size = minsonStringBuilderSize(b_);
    __value_buffer = m_new(char, value_size + 1);
    
    minsonBuildString(b_, __value_buffer);
    __value_buffer[value_size] = '\0';
    return __value_buffer;
}


int main(int argc, char *const *argv)
{
    _TEST_(argc == 2);
    
    FILE *file_in = fopen(argv[1], "rt");
    _TEST_(file_in != NULL);
    
    struct _base_context *context = m_pcast(struct _base_context *, _new_parse_stream(file_in));
    do
    {
        _TEST_(_parse_next(context) == __PARSE_OK);
        
        switch(context->token_type)
        {
        case _C_IDENTIFIER:
            printf("Identifier \t[%s]\n", __new_value(context->token_value));
            break;
            
        case _C_STRING:
            printf("String     \t[%s]\n", __new_value(context->token_value));
            break;
            
        case _C_BASIC:
        {
            printf("Basic      \t");
            
            __new_value(context->token_value);
            switch(_detect_basic_type(__value_buffer))
            {
            case _C_BASIC_WRONG_TYPE:
                _FAIL_("Wrong basic value.");
                
            case _C_BASIC_TRUE:
                printf("(boolean)[yes]\n"); break;
                
            case _C_BASIC_FALSE:
                printf("(boolean)[no]\n"); break;
                
            case _C_BASIC_NULL:
                printf("(null)\n"); break;
                
            case _C_BASIC_NUMBER:
            {
                int64_t number__;
                _TEST_(_parse_number(__value_buffer, &number__) == __PARSE_OK);
                printf("(number)[%ld]\n", number__);
                break;
            }
            case _C_BASIC_FLOAT:
            {
                double float__;
                _TEST_(_parse_float(__value_buffer, &float__) == __PARSE_OK);
                printf("(float-number)[%g]\n", float__);
                break;
            }
            }
            break;
        }
        case _C_ARRAY_BEGIN:
            printf("Array begin\t{\n"); break;
            
        case _C_ARRAY_END:
            printf("Array end  \t}\n"); break;
            
        case _C_END:
            printf("END\n"); break;
        }
    }
    while(context->token_type != _C_END);
    
    _delete_parse_stream(m_pcast(struct _t_stream_context *, context));
    fclose(file_in);
    
    if(__value_buffer != NULL) m_delete(__value_buffer);
    _ENDTEST_;
}
