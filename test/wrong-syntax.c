#include "minson.h"
#include "test.h"


int main(int argc, char *const *argv)
{
    _TEST_(argc == 1);
    
    t_minson_object *object = minsonParse("{qwerty}");
    _TEST_(object == NULL);
    _TEST_(minson_error_code == MINSON_ERROR_WRONG_SYNTAX);
    _TEST_(minson_error_line == 1);
    _TEST_(minson_error_column == 8);
    
    _ENDTEST_;
}
